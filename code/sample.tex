\documentclass[12pt]{article}

\usepackage{preamble}

\title{Sample}
\author{}

\begin{document}
\maketitle
\tableofcontents

\clearpage

\section{Preamble}

The \emph{preamble} is the part of the \TeX\ file preceding the line
\verb|\begin{document}|.

\vspace{\baselineskip}

It contains all the packages and \emph{macros} (the specially defined commands) to be used in the document.

\vspace{\baselineskip}

Add any packages or macros that you want, but also include everything in the preamble for the \TeX\ file for \emph{this} sample document.

\clearpage

\section{Simple exercises and labels}

To establish a common language, let's say that the workbook is composed of \emph{exercises}, and each exercise may be composed of \emph{tasks}.

\vspace{\baselineskip}

We use numbers to label exercises and lowercase letters to label tasks.

\subsection{Code for a simple exercise}

Here is the basic code for an exercise:

\begin{verbatim}
  \begin{exercise}
  Find the product of the binomials $2x - 3$ and $5x + 1$.
  \end{exercise}
\end{verbatim}

The output looks like this:

\fbox{
\begin{minipage}{.9\textwidth}
  \vspace{\baselineskip}
  \setcounter{exercise}{3}
  \begin{exercise}
  Find the product of the binomials $2x - 3$ and $5x + 1$.
  \end{exercise}
  \vspace{\baselineskip}
\end{minipage}
}

\subsection{Code for a simple exercise with a descriptive label}

Use the macro \verb|\exclabel{*}| to add a descriptive label for an exercise:

\begin{verbatim}
  \begin{exercise}
  \exclabel{Multiply binomials}
  Find the product of the binomials $2x - 3$ and $5x + 1$.
  \end{exercise}
\end{verbatim}

The output looks like this:

\fbox{
\begin{minipage}{.9\textwidth}
  \vspace{\baselineskip}
  \setcounter{exercise}{3}
  \begin{exercise}
  \exclabel{Multiply binomials}
  Find the product of the binomials $2x - 3$ and $5x + 1$.
  \end{exercise}
  \vspace{\baselineskip}
\end{minipage}
}

\clearpage

\section{Exercises with tasks}

\subsection{Code for an exercise with tasks aligned vertically}

Here is the code for an exercise composed of tasks, where each task appears on a new line:

\begin{verbatim}
  \begin{exercise}
  Write each equation in standard form.
    \begin{tasks}
    \task $3y - 2x = 8$
    \task $4x = 7y - 3$
    \task $5x + 1 = 3y$
    \end{tasks}
  \end{exercise}
\end{verbatim}

The output looks like this:

\fbox{
\begin{minipage}{.9\textwidth}
  \setcounter{exercise}{3}
  \vspace{\baselineskip}
  \begin{exercise}
  Write each equation in standard form.
    \begin{tasks}
    \task $3y - 2x = 8$
    \task $4x = 7y - 3$
    \task $5x + 1 = 3y$
    \end{tasks}
  \end{exercise}
  \vspace{\baselineskip}
\end{minipage}
}

\subsection{Code for an exercise with tasks aligned horizontally}

Here is the code for an exercise composed of tasks, where tasks appear in the same line:

\begin{verbatim}
  \begin{exercise}
  Write each equation in standard form.
    \begin{htasks}
    \task $3y - 2x = 8$
    \task $4x = 7y - 3$
    \task $5x + 1 = 3y$
    \task $6y + 8 = y$
    \end{htasks}
  \end{exercise}
\end{verbatim}

The output looks like this:

\fbox{
\begin{minipage}{.9\textwidth}
  \setcounter{exercise}{3}
  \vspace{\baselineskip}
  \begin{exercise}
  Write each equation in standard form.
    \begin{htasks}
    \task $3y - 2x = 8$
    \task $4x = 7y - 3$
    \task $5x + 1 = 3y$
    \task $6y + 8 = y$
    \end{htasks}
  \end{exercise}
  \vspace{\baselineskip}
\end{minipage}
}

\clearpage

\section{Multiple choice}

\subsection{Code for multiple choice options aligned vertically}

Here is the code for a multiple choice question with choices aligned vertically:

\begin{verbatim}
  \begin{exercise}
  Which number is prime?
    \begin{mc}
    \choice $4$
    \choice $8$
    \choice $9$
    \choice $11$
    \end{mc}
  \end{exercise}
\end{verbatim}

The output looks like this:

\fbox{
\begin{minipage}{.9\textwidth}
  \setcounter{exercise}{3}
  \vspace{\baselineskip}
  \begin{exercise}
  Which number is prime?
    \begin{mc}
    \choice $4$
    \choice $8$
    \choice $9$
    \choice $11$
    \end{mc}
  \end{exercise}
  \vspace{.5\baselineskip}
\end{minipage}
}

\subsection{Code for multiple choice options aligned horizontally}

Here is the code for a multiple choice question with choices aligned vertically:

\begin{verbatim}
  \begin{exercise}
  Which number is prime?
    \begin{hmc}
    \choice $4$
    \choice $8$
    \choice $9$
    \choice $11$
    \end{hmc}
  \end{exercise}
\end{verbatim}

The output looks like this:

\fbox{
\begin{minipage}{.9\textwidth}
  \setcounter{exercise}{3}
  \vspace{\baselineskip}
  \begin{exercise}
  Which number is prime?
    \begin{hmc}
    \choice $4$
    \choice $8$
    \choice $9$
    \choice $11$
    \end{hmc}
  \end{exercise}
  \vspace{.5\baselineskip}
\end{minipage}
}

\clearpage

\section{Comments and context}

\begin{itemize}
  \item To add a \textbf{comment}, use the macro \verb|\comment{}|
  \item To make a note of a \textbf{prerequisite} skill, use the macro \verb|\prereq{}|
  \item To \textbf{cite} a relevant example in \emph{Essentials for Algebra}, use the macro \verb|\zig{}|
  \item To make a note of key \textbf{vocabulary} to be taught, use the macro \verb|\vocab{}|
\end{itemize}

\clearpage

\section{Graphs}

Here is the code for the graph of the function $f(x) = -2x + 3$:

\begin{verbatim}
\begin{tikzpicture}[scale=.8] 
  \begin{axis}[ 
    grid style={line width=1pt, draw=gray!90}, 
    major grid style={line width=.2pt, draw=gray!90}, 
    axis lines=center, 
    axis line style={Latex[round]-Latex[round], line width=2.5pt}, 
    grid=both, 
    xmin=-4, 
    xmax=6, 
    ymin=-3, 
    ymax=7, 
    xtick={-10, ...,10}, 
    ytick={-10, ...,10}, 
    xlabel={$x$}, 
    ylabel={$y$}, 
    ] 
    \addplot[line width=3pt, samples=201, blue, domain=-2:3]{-2*x + 3}; 
  \end{axis} 
\end{tikzpicture}
\end{verbatim}

Things you may want to adjust:
\begin{itemize}[itemsep=0ex, topsep=1ex]
  \item the function: type the expression in the curly braces in the \verb|addplot| line
  \item the domain of the function: adjust the values after \verb|domain| in the \verb|addplot| line
  \item the range of $x$-values: adjust the \verb|xmin| and \verb|xmax| values
  \item the range of $y$-values: adjust the \verb|ymin| and \verb|ymax| values
  \item the size of the graph: adjust the scale in the first line ($.8$ means $80\%$ scale)
\end{itemize}

\clearpage

\section{Tables}

\subsection{Plain tables}

Here is the code for a table with four columns separated by vertical lines with text centered in cells:

\begin{verbatim}
\begin{tabular}{c|c|c|c}
$x$ & $y$ & $z$ & $f(x)$ \\
\hline
$7$ & $-1$ & $-3$ & $\frac{2}{3}$ \\
\end{tabular}
\end{verbatim}

\fbox{
\begin{minipage}{.9\textwidth}
  \vspace{\baselineskip}
\begin{tabular}{c|c|c|c}
$x$ & $y$ & $z$ & $f(x)$ \\
\hline
$7$ & $-1$ & $-3$ & $\frac{2}{3}$ \\
\end{tabular}
  \vspace{\baselineskip}
\end{minipage}
}

\vspace{\baselineskip}

What the code means:
\begin{itemize}[itemsep=0ex, topsep=1ex]
  \item the \verb|c|'s in the first line mean ``centered''
  \item to have left- or right-aligned cells, use \verb|l| or \verb|r|
  \item the \verb|&| symbol separates cells in a row
  \item the \verb|\\| ends each row
  \item the optional \verb|\hline| draws a separating line between rows
\end{itemize}

\clearpage

\subsection{Tables with slope arrows}

Here is the code for a table with two rows and arrows for slope computations:

\begin{verbatim}
\begin{tikzpicture}[style={line width=.4pt}]
  \matrix (mat) [matrix of nodes,
  nodes in empty cells,
  column sep=-\pgflinewidth,
  row sep=-\pgflinewidth,
  nodes={draw,
    text depth=.4ex,
    text height=1.2ex,
    minimum width=2em}] {
    $x$ & $y$ \\
    $1$ & $2$ \\
    $5$ & $9$ \\
  };
  \draw[line width=1pt, -{To}] 
    (mat-2-1.190) [bend right, xshift=-2em, yshift=1.5ex] 
    to (mat-3-1.170) node[left, xshift=-1em, yshift=1.5ex] {$+4$};
  \draw[line width=1pt, -{To}] 
    (mat-2-2.350) [bend left, xshift=2em, yshift=1.5ex] 
    to (mat-3-2.10) node[right, xshift=1em, yshift=1.5ex] {$+7$};
\end{tikzpicture}
\end{verbatim}

\begin{minipage}{.9\textwidth}
  \vspace{\baselineskip}
\begin{tikzpicture}[style={line width=.4pt}]
  \matrix (mat) [matrix of nodes,
  nodes in empty cells,
  column sep=-\pgflinewidth,
  row sep=-\pgflinewidth,
  nodes={draw,
    text depth=.4ex,
    text height=1.2ex,
    minimum width=2em}] {
    $x$ & $y$ \\
    $1$ & $2$ \\
    $5$ & $9$ \\
  };
  \draw[line width=1pt, -{To}] 
    (mat-2-1.190) [bend right, xshift=-2em, yshift=1.5ex] 
    to (mat-3-1.170) node[left, xshift=-1em, yshift=1.5ex] {$+4$};
  \draw[line width=1pt, -{To}] 
    (mat-2-2.350) [bend left, xshift=2em, yshift=1.5ex] 
    to (mat-3-2.10) node[right, xshift=1em, yshift=1.5ex] {$+7$};
\end{tikzpicture}
  \vspace{\baselineskip}
\end{minipage}

\clearpage

Here is the code for a table with three rows and arrows for slope computations:

\begin{verbatim}
\begin{tikzpicture}[style={line width=.4pt}]
  \matrix (mat) [matrix of nodes,
  nodes in empty cells,
  column sep=-\pgflinewidth,
  row sep=-\pgflinewidth,
  nodes={draw,
    text depth=.4ex,
    text height=1.2ex,
    minimum width=2em}] {
    $x$ & $y$ \\
    $1$ & $2$ \\
    $5$ & $9$ \\
    $7$ & $17$ \\
  };
  \draw[line width=1pt, -{To}] 
    (mat-2-1.190) [bend right, xshift=-2em, yshift=1.5ex] 
    to (mat-3-1.170) node[left, xshift=-1em, yshift=1.5ex] {$+4$};
  \draw[line width=1pt, -{To}] 
    (mat-3-1.190) [bend right, xshift=-2em, yshift=1.5ex] 
    to (mat-4-1.170) node[left, xshift=-1em, yshift=1.5ex] {$+2$};
  \draw[line width=1pt, -{To}] 
    (mat-3-2.350) [bend left, xshift=2em, yshift=1.5ex] 
    to (mat-3-2.10) node[right, xshift=1em, yshift=1.5ex] {$+7$};
  \draw[line width=1pt, -{To}] 
    (mat-3-2.350) [bend left, xshift=2em, yshift=1.5ex] 
    to (mat-4-2.10) node[right, xshift=1em, yshift=1.5ex] {$+8$};
\end{tikzpicture}
\end{verbatim}

\begin{minipage}{.9\textwidth}
  \vspace{\baselineskip}
\begin{tikzpicture}[style={line width=.4pt}]
  \matrix (mat) [matrix of nodes,
  nodes in empty cells,
  column sep=-\pgflinewidth,
  row sep=-\pgflinewidth,
  nodes={draw,
    text depth=.4ex,
    text height=1.2ex,
    minimum width=2em}] {
    $x$ & $y$ \\
    $1$ & $2$ \\
    $5$ & $9$ \\
    $7$ & $17$ \\
  };
  \draw[line width=1pt, -{To}] (mat-2-1.190) [bend right, xshift=-2em, yshift=1.5ex] to    (mat-3-1.170) node[left, xshift=-1em, yshift=1.5ex] {$+4$};
  \draw[line width=1pt, -{To}] 
    (mat-3-1.190) [bend right, xshift=-2em, yshift=1.5ex] 
    to (mat-4-1.170) node[left, xshift=-1em, yshift=1.5ex] {$+2$};
  \draw[line width=1pt, -{To}] (mat-2-2.350) [bend left, xshift=2em, yshift=1.5ex] to (mat-3-2.10) node[right, xshift=1em, yshift=1.5ex] {$+7$};
  \draw[line width=1pt, -{To}] 
    (mat-3-2.350) [bend left, xshift=2em, yshift=1.5ex] 
    to (mat-4-2.10) node[right, xshift=1em, yshift=1.5ex] {$+8$};
\end{tikzpicture}
  \vspace{\baselineskip}
\end{minipage}

\clearpage

\subsection{Horizontal table with slope arrows}

\begin{verbatim}
\begin{tikzpicture}[style={line width=.4pt}]
  \matrix (mat) [matrix of nodes,
  nodes in empty cells,
  column sep=-\pgflinewidth,
  row sep=-\pgflinewidth,
  nodes={draw,
    text depth=.4ex,
    text height=1.2ex,
    minimum width=2em}] {
      $x$ & $3$ & $7$ & $10$ \\
      $y$ & $5$ & $11$ & $13$ \\
  };
  \draw[line width=1pt, -{To}] 
    (mat-1-2.80) [bend left, yshift=3ex] 
    to (mat-1-3.100) node[left, yshift=3ex] {$+4$};
  \draw[line width=1pt, -{To}] 
    (mat-1-3.80) [bend left, yshift=3ex] 
    to (mat-1-4.100) node[left, yshift=3ex] {$+3$};
  \draw[line width=1pt, -{To}] 
    (mat-2-2.280) [bend right, yshift=-3ex] 
    to (mat-2-3.260) node[left, yshift=-3ex] {$+6$};
  \draw[line width=1pt, -{To}] 
    (mat-2-3.280) [bend right, yshift=-3ex] 
    to (mat-2-4.260) node[left, yshift=-3ex] {$+2$};
\end{tikzpicture}
\end{verbatim}

\begin{tikzpicture}[style={line width=.4pt}]
  \matrix (mat) [matrix of nodes,
  nodes in empty cells,
  column sep=-\pgflinewidth,
  row sep=-\pgflinewidth,
  nodes={draw,
    text depth=.4ex,
    text height=1.2ex,
    minimum width=2em}] {
      $x$ & $3$ & $7$ & $10$ \\
      $y$ & $5$ & $11$ & $13$ \\
  };
  \draw[line width=1pt, -{To}] 
    (mat-1-2.80) [bend left, yshift=3ex] 
    to (mat-1-3.100) node[left, yshift=3ex] {$+4$};
  \draw[line width=1pt, -{To}] 
    (mat-1-3.80) [bend left, yshift=3ex] 
    to (mat-1-4.100) node[left, yshift=3ex] {$+3$};
  \draw[line width=1pt, -{To}] 
    (mat-2-2.280) [bend right, yshift=-3ex] 
    to (mat-2-3.260) node[left, yshift=-3ex] {$+6$};
  \draw[line width=1pt, -{To}] 
    (mat-2-3.280) [bend right, yshift=-3ex] 
    to (mat-2-4.260) node[left, yshift=-3ex] {$+2$};
\end{tikzpicture}

\clearpage

\section{Special notation}

\subsection{Answer blanks}

There are two types of answer blanks.
The default versions are wide enough for an answer that is one or two characters wide.

\begin{minipage}{.4\textwidth}
\begin{verbatim}
The answer is $x = \blank$

The answer is $x = \blankbox$
\end{verbatim}
\end{minipage}
\hfill
\begin{minipage}{.35\textwidth}
The answer is $x = \blank$

The answer is $x = \blankbox$
\end{minipage}

\vspace{2\baselineskip}

If the answer requires more than two characters, specify the width by appending a numerical value in square brackets to the command: 

\begin{minipage}{.43\textwidth}
\begin{verbatim}
The answer is $x = \blank[3]$

The answer is $x = \blankbox[5]$
\end{verbatim}
\end{minipage}
\hfill
\begin{minipage}{.53\textwidth}
The answer is $x = \blank[3]$

The answer is $x = \blankbox[5]$
\end{minipage}

\vspace{2\baselineskip}

There are also versions for fractions:

\begin{minipage}{.35\textwidth}
\begin{verbatim}
$x = \blankboxfraction$
\end{verbatim}
\end{minipage}
\hfill
\begin{minipage}{.5\textwidth}
$x = \blankboxfraction$
\end{minipage}

\begin{minipage}{.35\textwidth}
\begin{verbatim}
$\blankfraction$
\end{verbatim}
\end{minipage}
\hfill
\begin{minipage}{.5\textwidth}
$x = \blankfraction$
\end{minipage}

\begin{minipage}{.35\textwidth}
\begin{verbatim}
$x = \blankfraction[4]$
\end{verbatim}
\end{minipage}
\hfill
\begin{minipage}{.5\textwidth}
$x = \blankfraction[4]$
\end{minipage}

\begin{minipage}{.35\textwidth}
\begin{verbatim}
$x = \blankboxfraction[6]$
\end{verbatim}
\end{minipage}
\hfill
\begin{minipage}{.5\textwidth}
$x = \blankboxfraction[6]$
\end{minipage}
\end{document}
