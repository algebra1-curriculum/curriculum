\begin{tikzpicture}[scale=.8] 
  \begin{axis}[ 
    grid style={line width=1pt, draw=gray!90}, 
    major grid style={line width=.2pt, draw=gray!90}, 
    axis lines=center, 
    axis line style={Latex[round]-Latex[round], line width=2.5pt}, 
    grid=both, 
    xmin=-2, 
    xmax=3, 
    ymin=-3, 
    ymax=7, 
    xtick={-10, ...,10}, 
    ytick={-10, ...,10}, 
    xlabel={$x$}, 
    ylabel={$y$}, 
    ] 
    \addplot[<->, line width=2pt, samples=501, blue, domain=-2:3]{-2*x + 3} node[above     right, pos=.5, fill=white] {$y = g(x)$}; 
  \end{axis} 
\end{tikzpicture}
