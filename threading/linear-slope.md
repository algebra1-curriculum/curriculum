# Slope

* calculate $\Delta x$ and $\Delta y$ from a table
  - only two rows in first examples
  - draw the arrows
  - subtract
  - scaffold: $\Delta x = ?$, $\Delta y = ?$
  - rigor and number sense: negative values, decimal values

* find ``simple slopes'' from table
  - given table with $\Delta x$ and $\Delta y$ labeled, find slope
  - given table with two rows, find $\Delta x$ and $\Delta y$, then find slope

* find simple slope given two ordered pairs
  - make the table first
  - find $\Delta x$ and $\Delta y$
  - find the slope

* given graph with two marked points, find simple slope
  - make the table first
  - find the simple slope
  - scaffold: start with labeled points

* given graph with no marked points, find the slope
  - mark two points (``corner of a square'')
  - make the table
  - find the simple slope

* given a table with three rows, find the simple slopes and graph the line (segments)
  - start with two identical simple slopes (not integers)
  - then two unequal simple slopes
  - then two simple slopes that are reciprocals
  - then two simple slopes that are equivalent fractions

* given a table with several rows, find simple slopes and determine if graph is linear without graphing it

* given three ordered pairs, determine if they are collinear
  - make a table
  - find simple slopes
  - compare
