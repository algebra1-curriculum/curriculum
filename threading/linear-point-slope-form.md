# Point-slope form

* given $y = \frac{4}{3}(x - 5) + 7$
  - what's $h$? $k$? $\Delta x$? $\Delta y$?
  - write the ordered pair
  - boundary cases
    * integer slope
    * negative $h$ and $k$
    * slope equals 1
    * $h = 0$ or $k = 0$

* given $y = \frac{4}{3}(x - 5) + 7$, make a table of values
  - fill in $(h,k)$
  - fill in $\Delta x$ and $\Delta y$ with arrows
  - find next point
  - graph

* special case: slope-intercept form
  - after the ``Features of graphs'' thread
