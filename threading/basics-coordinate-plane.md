# Coordinate plane

* vocab: origin, axes, point, coordinate, intersection, ordered pair

* graphing vertical and horizontal lines:
  - directions indicate mini-whiteboards
  - graph vertical line given equation $x = 3$
  - graph horizontal line given equation $y = 5$
  - mixed practice with positive values
  - marking the intersection
  - special cases: $x = 0$ and $y = 0$

* graphing ordered pairs:
  - given $x = 3$ and $y = 5$, graph both, then mark the intersection point
  - special case: points on axes
  - given $(5,2)$, write the two equations $x = 5$ and $y = 2$, then plot the point

* given vertical and horizontal lines, write the equations

* given plotted point:
  - draw the lines
  - then write the equation

* given several plotted points:
  - write the equations
  - then write the ordered pairs

* repeat with negative values
