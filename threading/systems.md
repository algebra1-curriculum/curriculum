# Systems

## Elimination method

* make the integers opposite by multiplying

* adding linear equations
  - start with two standard-form linear equations written in columns
  - covertize coefficients
  - rearrange terms

* multiplying equations by constants
* multiplying equations by different values then adding them together

* add equations that are ready for elimination

* discrimination: ready or not ready? 

* solve ready systems

* make system ready
  - multiply top by 3 and bottom by -7
  - multiply top by 3 and -7, then add
  - make the system ready by multiplying both equations

* solve not-ready systems
  - choose a letter to eliminate
  - multiply both equations
  - eliminate
  - solve

## Super-ready substitution

* write the single equation
  - mix in slope-intercept form, function notation, quadratics
  - discrimination: are we solving for x, y (or f(x)), or both?

* determine the number of solutions without solving
  - discrimination: are these lines intersecting?
  - discrimination: are these non-intersecting lines parallel?
  - practice with equivalent fractions
