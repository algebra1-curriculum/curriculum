# Features of graphs

* given graphs of lines, parabolas, etc.:
  - find the x-intercepts, y-intercepts
  - find minimum or maximum
  - find increasing/decreasing intervals
  - find the positive/negative intervals
  - will this line have an x-intercept? y-intercept?
  - what's the end behavior?

* find the y-intercept algebraically:
  - insist that y-intercept has two coordinates: $(0,4)$, not just $4$
  - point-slope form
  - standard form linear
  - slope-intercept form (before giving that form a name)
  - quadratics
