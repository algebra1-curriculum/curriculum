# Quadratic graphs: factored form

* given factored form, find $a$, $r$, $s$

* given graph, find $r$ and $s$

* given graph, find axis of symmetry

* given graph, write equation for axis: $x = h$

* given equation, find $h = (r+s)/2$

* given graph, find $h$ when it's a decimal by finding $r$ and $s$ first

* given graph, find $k$

* given $h$ and equation, find $k$

* given equation, find $h$, then $k$

* find maximum/minimum

* given equation, find $y$-intercept

* terminal atom: graph from factored form including intercepts and vertex

* terminal atom: given $a$ and graph, write the equation
