# Solving equations

* one-step, two-step
  - reciprocals not division
  - follow Engelmann
  - possibly less emphasis on fractions

* substitute into two-variable two-step equation
  - Engelmann, Lesson 37
  - $3m + 2 = 5j + 1$, $j = 7$

* multi-step:
  - Engelmann, Lesson 48
  - start with a side with a letter term and a number term and eliminate one of the terms from that side

* $3j + 5 = 6j - 7$
* $3(x + 5) = 6x - 7$

* $\frac{x + 5}{6} = 4x + 1$
* $\frac{x + 5}{6} = \frac{4x + 1}{3}$
$ $\frac{7(x + 5)}{6} = 10$
