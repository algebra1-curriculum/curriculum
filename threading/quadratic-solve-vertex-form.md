# Vertex form: solve

* evaluating perfect squares and square roots

* order of operations: $(-3)^2$ vs $-3^2$

* solve $7 = p^2$
  - $\pm sqrt{7} = p$
  - $p = \sqrt{7}$ or $p = -\sqrt{7}$
  - insist that students write one equation for each solution

* $\pm$ arithmetic
  - $8 \pm 6$
  - $10 \pm 12$
  - $x + 3 = \pm 5$
    * $x + 3 = 5$ or $x + 3 = -5$

* solve $r^2 + 8 = 24$

* solve $(x+5)^2 = 100$

* solve $(x+5)^2 - 1 = 81$

* solve $\frac{1}{3}(x - 1)^2 + 10 = 27$
