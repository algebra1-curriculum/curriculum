# Quadratics: solve from factored form

* zero-product property
  - start with fully overt example: $6(2x + 3)(5x - 7) = 0$

* special cases
  - $5(x + 6)(x - 8) = 0$
  - $(x + 2)(x - 4) = 0$
  - $3x(x + 5) = 0$
  - $x(x-1) = 0$

* consolidation practice:
  - $(x - 3)(x + 6)(x - 7)^2 = 0$
  - after students are proficient at factoring: $(x - 3)(x^2 + 6x + 8) = 0$
