# Language of functions

* given equations $f(x) = 5$, $h(t) = t + 5$, $g(4) = 7$:
  - say it the math way:  ``f of x equals 5''
  - say it the English way: ``when the input is x, then the output is 5''

* given a table:
  - say it the math way for Row 3
  - say it the English way

* given a list of ordered pairs:
  - say it the math way for the second ordered pair
  - say it the English way

* given a graph:
  - say it the math way for the second ordered pair
  - say it the English way
