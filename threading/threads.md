# Functions

* output from input: $f(7) = ?$
  - ordered pair
  - table
  - graph
  - formula (substitution)
  - context

* input(s) from output: $f(?) = 5$
  - ordered pair
  - table
  - graph
  - formula
  - context

* find input/output values for which $f(x) = g(x)$

* parent functions (introduce early in the year)

* substitution

* transformations

# Inequalities

* symbols (very early)

* number sense
  - 3 > 4?
  - 3 > 3.1?
  - 0.8 > 0.09?

* one variable

* two variables, one inequality

* two variables, two inequalities

* context

* language skills
  - at least, at most
  - does not exceed

# Polynomial arithmetic

* signed integer arithmetic

* reciprocals

* exponents

* like terms
  - combining signed like terms
  - multiplying monomials

* distributive property
  - expanding brackets

* adding polynomials

* clearing denominators

* fractions and decimals as coefficients

# Linear relationships

* slope
  - overtize $m = j/r$, $j$ for jump, $r$ for run?

* point-slope form from:
  - two points
  - table
  - graph
  - context 

* standard form
  - convert
  - graph
  - context

* initial value
  - $h = 0$ vs $y = mx + b$

* ADD TO SLOPE THREAD:
  - given point and slope, find two more points in the table

* reasoning with quadrants and intercepts

# Systems of equations

* adding polynomials

* graphical solutions
  - number of solutions
  - this should be treated previously in the features of graphs thread

* elimination
  - make it ready

* super-ready substitution
  - $y = mx + b$
  - $f(x) = mx + b$

* method selection

* less emphasis on perpendicular lines

# Quadratic relationships

* parent function

* vertex form
  - parabolas
  - transformations

* operations and notation
  - square roots
  - plus-or-minus
  - be explicit about showing work for $x + 4 = \pm 5$

* factored form
  - mininum and maximum
  - difference of squares
  - perfect square trinomials

* standard form
  - factor
  - complete the square
  - quadratic formula

# Exponential relationships

* percentages

* constant ratio
  - times pattern

* initial value

* find the formula $f(x) = c(m)^x$
  - find a better letter for multiplication than $m$?
  - two points
  - table
  - graph
  - context

* find input given output
