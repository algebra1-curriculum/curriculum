# Quadratics: solve from standard form by completing the square 

* perfect square trinomials
  - start with expanding squared binomials
  - start with overt case: $(3x+5)^2 = 9x^2 + 30x + 15$
  - then factor perfect square trinomials: $x^2 + 8x + 16 = (x + 4)^2$
  
* isolate the constant term

* compute $(b/2)^2$

* adding $(b/2)^2$ to both sides

* factoring

* consolidation: variables on both sides: $x^2 + 12x + 5 = 13 - 6x$

* if $x^2 + 12x + 1 = 3$ is equivalent to $(x+p)^2 = q$, find $q$

* find the vertex of $f(x) = x^2 + 8x + 3$ by completing the square
  - this overlaps with the quadratic features of graphs thread
