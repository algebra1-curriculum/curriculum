# Quadratic graphs: standard form

* given standard form, find $a$, $b$, $c$

* given graph, find $c$

* fluency with $-b/(2a)$:
  - given vertex form equation, what's $-h$? $5k$?
  - given standard form equation, what's $-3a$? $-b$?
  - given factored form equations, what's $-a$? $-r$? $3s$?

* given equation, find $h = -b/(2a)$

* given $h$ and equation, find $k$

* given equation, find $h$, then $k$

* find maximum/minimum

* match graph to equation based on $h$ and $k$

* given equation, find $x$-intercepts
  - depends on quadratic solving threads

* terminal atom: graph from standard form including intercepts and vertex

* post-terminal atom: given $a$ and graph, write the equation
