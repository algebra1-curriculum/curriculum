# Transformations

## Shapes and function notation

* vertical translation
  - right triangle: translation or not?
  - right triangle: identify the translation
  - right triangle: draw the translation
  - point: identify translation
  - point: draw translation
  - point: find coordinates after translation
  - function notation: identify translation
  - function notation: match graph to translation

* horizontal translation
  - right triangle: identify translation
    * mixed: vertical and horizontal examples
  - right triangle: draw the translation
  - point: identify translation
  - point: draw translation
  - point: find coordinates after translation
  - function notation: identify translation
  - function notation: match graph to translation

* vertical reflection
  - right triangle: identify the transformation 
  - right triangle: draw the reflection
  - point: identify transformation
  - point: draw reflection 
  - point: find coordinates after reflection
  - function notation: identify transformation
  - function notation: match graph to transformation

* horizontal reflection
  - right triangle: identify the transformation 
  - right triangle: draw the reflection
  - point: identify transformation
  - point: draw reflection 
  - point: find coordinates after reflection
  - function notation: identify transformation
  - function notation: match graph to transformation

## Absolute value
