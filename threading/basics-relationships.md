# Relationships 

* symbols
  - start with less than, greater than, and equals 
  - introduce the non-strict inequalities later

* given $3 < 5$:
  - say the sentence: ``three is less than five''
  - is the sentence true or false?
  - compare whole numbers
  - compare integers
  - compare fractions to one
  - compare fractions to whole numbers
    * division with remainder
  - compare decimals

* vocabulary:
  - more than/not more than
  - fewer than/no fewer than
  - at least/at most
  - not exceeding
  - introduce one pair per week

## One-variable inequalities

* given $3 < x$:
  - say the sentence: ``all numbers greater than 3''
  - $4 < x < 10$
  - list solutions: what numbers make this true?

* solve one-step inequalities
  - golden rule
  - answer format: ``all numbers greater than 5''

* multi-step
  - more golden rule practice
  - insiste on answer format

## Two-variable inequalities

* given $3x + 5y > 7$:
  - test solutions algebraically (substitute)

* graphing:
  - point-slope form
  - only greater than for first examples
  - is it ``ready to graph''? (is y isolated?)
  - then introduce less than
  - then introduce non-strict inequalities
  - make it ready to graph
    * focus on standard form

* given graph and several inequalities, choose the inequality that matches the graph
  - find the slope and y-intercept for each inequality
  - then compare

## Systems of two-variable inequalities

* given $y > 3$ and $y > \frac{1}{2}(x + 5) - 3$
  - match each inequality to line in graph of system 
  - then choose the solution set

* given two point-slope form inequalities, graph the solution set

* given one point-slope form and one standard form, graph the solution set

* given two standard-form inequalities, graph the solution set

## Modeling

* write the inequality for the scenario
  - one-variable
  - two-variables
  - systems of two-variable inequalities
