# Quadratic graphs: vertex form

* vertex form: find $a$, $h$, $k$

* given graph, find $(h,k)$

* given graph, find equation for axis of symmetry $x = h$

* match graph to vertex form
  - interpret $a$ (up/down/wide/narrow)

* transformations
  - vertical translation
  - horizontal translation
  - reflections

* find the vertex after applying transformations

* find the $x$-intercepts: replace $y$ by $0$, the solve
  - this continues the features of general graphs thread
  - this depends on the thread for solving from vertex form

* find $y$-intercept: replace $x$ by $0$, then simplify
  - this continues the features of general graphs thread

* terminal atom: graph from vertex form including $x$-intercepts, $y$-intercept, vertex

* terminal atom: given $a$ and graph, find $h$, $k$, then write equation 
