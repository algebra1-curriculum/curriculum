# Graphs of functions

* given function notation $f(2) = 5$
  - write the equations $x = 2$ and $y = 5$
  - scaffold: $x = ?$, $y = ?$

* given equation $f(3) = 7$
  - write the ordered pair
  - scaffold: $(?,?)$

* given equations $x = 2$ and $y = 9$
  - write the function notation $f(2) = 9$
  - scaffold: $f(?) = ?$
  - give the equations in both orders: $x$ first and $y$ first
  - use different letters for the function, input variable, and output variable

* given ordered pair $(5,3)$
  - write the function notation $f(5) = 3$

* given graph of function with several local extrema and one marked point:
  - write the equations $x = ?$ and $y = ?$
  - write the function notation
  - scaffolds: 
    * vertical and horizontal lines through the point
    * ordered pair for the point shown
    * $x = ?$ and $y = $
    * $f(?) = ?$
  - removing scaffolds:
    * remove ordered pairs
    * remove lines

* given graph of function with several local extrema and several marked points:
  - complete a table with missing values for $x$ and $f(x)$

* given graph of a function with no marked points:
  - complete the function notation $f(?) = 8$, $f(3) = ?$
  - replace the blanks with letters: $f(a) = 8$, so $a = ?$
