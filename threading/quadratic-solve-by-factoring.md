# Quadratics: solve from standard form by factoring

* complication: if we emphasize the distributive property over area models, the product-sum relationships between $b$ and $c$ are less apparent
  - (x + 3)(x + 5) = x(x+5) + 3(x+5)
  - = (x)(x) + (x)(5) + (3)(x) + (3)(5)
  - = x^2 + (5 + 3)x + (3)(5)

* motivation/pre-requisite: solving $(x + 4)(x + 5) = 0$ is easier that $x^2 + 9x + 20 = 0$

* $x^2 + 6x + 8 = 0$
  - $(x + 2)(x + 4) = 0$

* using the factor assistant
  - look at $c$: same signs or different?
  - look at $b$: 
    * (if $c>0$) both positive or both negative?
    * (if $c<0$) plus 1 or minus 1?

* $x^2 - 8x + 12 = 0$

* $x^2 + 3x - 70 = 0$

* $x^2 - 2x - 24 = 0$

* $x^2 - 2x - 20 = 4$
