* must complete before Summer Bridge

* Honors criteria:
  - (last year's grade, and?)
  - 730 on MCAP Math8, or
  - (placement test), or
  - teacher recommendation
    * especially ML students
  
* multiplication facts
* fraction vs decimal vs percent
* integer arithmetic: start with easy ones to boost motivation
* plotting points
* one-step equation: maybe one with a blank instead of a variable
* two-step equations
* Saturday School diagnostic: word problem
* area of a rectangle?
