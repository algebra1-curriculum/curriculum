rm Trans.1.tex
cat >Trans.1.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle the number with the larger absolute value.

\begin{tasks}[after-skip=0\baselineskip](4)
\task 
$\begin{cases}
-3 \\
-8 
\end{cases}$
\task 
$\begin{cases}
+7 \\
-6.5
\end{cases}$
\task 
$\begin{cases}
-8.5 \\
-2
\end{cases}$
\task 
$\begin{cases}
-\frac{1}{5} \\
-\frac{2}{5}
\end{cases}$
\end{tasks}
\end{document}
EOL
pdflatex Trans.1.tex
mv -f Trans.1.log ../log
mv -f Trans.1.tex ../tex
mv -f Trans.1.pdf ../pdf
	
rm Trans.10.tex
cat >Trans.10.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Evaluate each function at the given value.

\begin{tasks}[after-item-skip=6\baselineskip, after-skip=6\baselineskip](2)
\task $h(w) = -21 - \frac{6}{5}\abs{w + 4}$ \quad \fbox{$w = -14$}
\task $t(v) = -11\abs{v} + 3$ \quad \fbox{$v = -4$}
\task $g(x) = 3(x - 2) + 6.5$ \quad \fbox{$x = 1.3$}
\task $q(x) = \abs{-5x} - 7$ \quad \fbox{$x = 0.2$}
\end{tasks}
\end{document}
EOL
pdflatex Trans.10.tex
mv -f Trans.10.log ../log
mv -f Trans.10.tex ../tex
mv -f Trans.10.pdf ../pdf
	
rm Trans.11.tex
cat >Trans.11.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
If the graph has a vertex, write the equations for the coordinates of the vertex.

\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-4,
ymax=4,
]
\draw[blue, <->] (-4,-1.8) -- (2,-3) -- (4,-2.4);
\draw[red, fill=white] (3,3) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-4,
ymax=4,
]
\draw[blue, <->] (-2.4,-4) -- (-1,3) -- (0.4,-4);
\draw[red, fill=white] (3,3) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-4,
ymax=4,
]
\draw[blue, <->] (4,-4) -- (2,0) -- (0,-4);
\draw[red, fill=white] (3,3) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-4,
ymax=4,
]
\draw[blue, <->] (-4,3) -- (0,-3) -- (4,3);
\draw[red, fill=white] (3,3) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.11.tex
mv -f Trans.11.log ../log
mv -f Trans.11.tex ../tex
mv -f Trans.11.pdf ../pdf
	
rm Trans.12.tex
cat >Trans.12.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
If the graph has a vertex, write the ordered pair for the vertex.

\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-4,
ymax=4,
]
\draw[blue, <->] (-4,-3) -- (4,2);
\draw[red, fill=white] (3,3) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-6,
ymax=2,
]
\draw[blue, <->] (-4,-5) -- (-1,-2) -- (3,-6);
\draw[red, fill=white] (3,1) circle (1em) node {\large \stask \normalsize};
%\addplot[<->, line width=4pt, samples=201, blue, domain=-4:4]{-1*abs(x + 1) - 2};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-6,
ymax=2,
]
\draw[blue, <->] (-4,-2) -- (-3,-4) -- (0,2);
\draw[red, fill=white] (3,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-2,
ymax=6,
]
\draw[blue, <->] (-4,5) -- (2,3) -- (4,11/3);
\draw[red, fill=white] (3,5) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.12.tex
mv -f Trans.12.log ../log
mv -f Trans.12.tex ../tex
mv -f Trans.12.pdf ../pdf
	
rm Trans.13.tex
cat >Trans.13.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
If the graph has a minimum or maximum, find its value.

\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-1,
ymax=7,
]
\draw[blue, <->] (-1,2) -- (5,6) -- (7,16/3);
\draw[red, fill=white] (6,0) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-7,
xmax=1,
ymin=-4,
ymax=4,
]
\draw[blue, <->] (-7,1) -- (-4,2) -- (1,1/3);
\draw[red, fill=white] (0,-3) circle (1em) node {\large \stask \normalsize};
%\addplot[<->, line width=4pt, samples=201, blue, domain=-4:4]{-1*abs(x + 1) - 2};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-2,
ymax=6,
]
\draw[blue, <->] (-4,1) -- (2,3) -- (4,5);
\draw[red, fill=white] (3,-1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-2,
ymax=6,
]
\draw[blue, <->] (-4,2) -- (-3,4) -- (0,-2);
\draw[red, fill=white] (3,-1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.13.tex
mv -f Trans.13.log ../log
mv -f Trans.13.tex ../tex
mv -f Trans.13.pdf ../pdf
	
rm Trans.14.tex
cat >Trans.14.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
If the graph has a minimum or maximum, find its value.

\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-7,
ymax=1,
]
\draw[blue, <->] (-1,-1) -- (5,-6) -- (7,-26/6);
\draw[red, fill=white] (0,0) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-7,
ymax=1,
]
\draw[blue, <->] (-1,-5) -- (1,-2) -- (7,-1);
\draw[red, fill=white] (0,0) circle (1em) node {\large \stask \normalsize};
%\addplot[<->, line width=4pt, samples=201, blue, domain=-4:4]{-1*abs(x + 1) - 2};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-2,
ymax=6,
]
\draw[blue, <->] (-4,6) -- (0,1) -- (4,6);
\draw[red, fill=white] (-3,5) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.5]
\begin{axis}[
xmin=-4,
xmax=4,
ymin=-2,
ymax=6,
]
\draw[blue, <->] (-4,-1) -- (3,6) -- (4,5);
\draw[red, fill=white] (-3,5) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.14.tex
mv -f Trans.14.log ../log
mv -f Trans.14.tex ../tex
mv -f Trans.14.pdf ../pdf
	
rm Trans.15.tex
cat >Trans.15.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
If the graph of the function has a vertex, write it as an ordered pair $(h,k)$.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](3)
\task $p(x) = 5\abs{x - 2} + 7$
\task $q(t) = -4\abs{t - 9} - 3$
\task $t(z) = -4(z - 9) - 3$
\task $f(x) = -7\abs{x + 5} - 1$
\task $g(v) = 7(v + 3) + 2$
\task $w(x) = -\abs{x - 4} - 2$
\end{tasks}
\end{document}
EOL
pdflatex Trans.15.tex
mv -f Trans.15.log ../log
mv -f Trans.15.tex ../tex
mv -f Trans.15.pdf ../pdf
	
rm Trans.16.tex
cat >Trans.16.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
If the graph of the function has a vertex, write it as an ordered pair $(h,k)$.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](3)
\task $w(t) = -3\abs{t + 2} - 6$
\task $f(v) = 5\abs{v - 8} - 3$
\task $m(x) = 11(x + 4) - 8$
\end{tasks}
\end{document}
EOL
pdflatex Trans.16.tex
mv -f Trans.16.log ../log
mv -f Trans.16.tex ../tex
mv -f Trans.16.pdf ../pdf
	
rm Trans.17.tex
cat >Trans.17.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
If the graph of the function has a vertex, write it as an ordered pair $(h,k)$.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](3)
\task $g(x) = 3(x + 7) + 2$
\task $c(t) = -4\abs{t - 5}$
\task $m(x) = 2\abs{x} - 5$
\end{tasks}
\end{document}
EOL
pdflatex Trans.17.tex
mv -f Trans.17.log ../log
mv -f Trans.17.tex ../tex
mv -f Trans.17.pdf ../pdf
	
rm Trans.18.tex
cat >Trans.18.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
If the graph of the function has a vertex, write it as an ordered pair $(h,k)$.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](3)
\task $f(z) = -\abs{z + 2}$
\task $d(t) = -4\abs{t - 9} - 3$
\task $m(x) = 2\abs{x - 5}$
\end{tasks}
\end{document}
EOL
pdflatex Trans.18.tex
mv -f Trans.18.log ../log
mv -f Trans.18.tex ../tex
mv -f Trans.18.pdf ../pdf
	
rm Trans.19.tex
cat >Trans.19.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find the vertex.
Then find the right slope.
Then find the left slope.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-1,
ymax=7,
]
\draw[blue, <->] (-1,6) -- (3,4) -- (7,6);
\filldraw[blue] (1,5) circle (4pt);
\filldraw[blue] (3,4) circle (4pt);
\filldraw[blue] (5,5) circle (4pt);
\draw[red, fill=white] (6,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-1,
ymax=7,
]
\draw[blue, <->] (0,7) -- (3,1) -- (6,7);
\filldraw[blue] (1,5) circle (4pt);
\filldraw[blue] (3,1) circle (4pt);
\filldraw[blue] (5,5) circle (4pt);
\draw[red, fill=white] (6,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-1,
ymax=7,
]
\draw[blue, <->] (-2/3,-1) -- (4,6) -- (7,1.5);
\filldraw[blue] (2,3) circle (4pt);
\filldraw[blue] (4,6) circle (4pt);
\filldraw[blue] (6,3) circle (4pt);
\draw[red, fill=white] (6,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{4\baselineskip}
\end{document}
EOL
pdflatex Trans.19.tex
mv -f Trans.19.log ../log
mv -f Trans.19.tex ../tex
mv -f Trans.19.pdf ../pdf
	
rm Trans.2.tex
cat >Trans.2.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle the number with the larger absolute value.

\begin{tasks}[after-skip=0\baselineskip](4)
\task 
$\begin{cases}
-5 \\
-5.9 
\end{cases}$
\task 
$\begin{cases}
-12 \\
+5
\end{cases}$
\task 
$\begin{cases}
\frac{7}{3} \\
-\frac{2}{3}
\end{cases}$
\task 
$\begin{cases}
+17.8 \\
+18
\end{cases}$
\end{tasks}
\end{document}
EOL
pdflatex Trans.2.tex
mv -f Trans.2.log ../log
mv -f Trans.2.tex ../tex
mv -f Trans.2.pdf ../pdf
	
rm Trans.20.tex
cat >Trans.20.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find the vertex.
Then find the right slope.
Then find the left slope.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=9,
ymin=-1,
ymax=9,
]
\draw[blue, <->] (-1,8.5) -- (2,1) -- (5.2,9);
\filldraw[blue] (0,6) circle (4pt);
\filldraw[blue] (2,1) circle (4pt);
\filldraw[blue] (4,6) circle (4pt);
\draw[red, fill=white] (8,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=9,
ymin=-1,
ymax=9,
]
\draw[blue, <->] (3,-1) -- (7,7) -- (9,3);
\filldraw[blue] (5,3) circle (4pt);
\filldraw[blue] (7,7) circle (4pt);
\filldraw[blue] (8,5) circle (4pt);
\draw[red, fill=white] (8,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=9,
ymin=-1,
ymax=9,
]
\draw[blue, <->] (-1,4.5) -- (4,2) -- (9,4.5);
\filldraw[blue] (2,3) circle (4pt);
\filldraw[blue] (4,2) circle (4pt);
\filldraw[blue] (8,4) circle (4pt);
\draw[red, fill=white] (8,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{4\baselineskip}
\end{document}
EOL
pdflatex Trans.20.tex
mv -f Trans.20.log ../log
mv -f Trans.20.tex ../tex
mv -f Trans.20.pdf ../pdf
	
rm Trans.21.tex
cat >Trans.21.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Write the vertex-form equation for each graph.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-2,
ymax=8,
]
\draw[blue, <->] (-3.5,8) -- (1,2) -- (5,22/3);
\filldraw[blue] (-2,6) circle (4pt);
\filldraw[blue] (1,2) circle (4pt);
\filldraw[blue] (4,6) circle (4pt);
\draw[red, fill=white] (4,-1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-3,
xmax=7,
ymin=-2,
ymax=8,
]
\draw[blue, <->] (-0.5,8) -- (3,1) -- (6.5,8);
\filldraw[blue] (3,1) circle (4pt);
\filldraw[blue] (4,3) circle (4pt);
\draw[red, fill=white] (6,-1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-8,
xmax=2,
ymin=-5,
ymax=5,
]
\draw[blue, <->] (-8,-3.5) -- (-3,-1) -- (2,-3.5);
\filldraw[blue] (-5,-2) circle (4pt);
\filldraw[blue] (-3,-1) circle (4pt);
\draw[red, fill=white] (1,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{4\baselineskip}
\end{document}
EOL
pdflatex Trans.21.tex
mv -f Trans.21.log ../log
mv -f Trans.21.tex ../tex
mv -f Trans.21.pdf ../pdf
	
rm Trans.22.tex
cat >Trans.22.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Write the vertex-form equation for each graph.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-3,
xmax=7,
ymin=-2,
ymax=8,
]
\draw[blue, <->] (1,8) -- (3,4) -- (5,8);
\draw[red, fill=white] (6,-1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-3,
xmax=7,
ymin=-2,
ymax=8,
]
\draw[blue, <->] (-3,14/3) -- (2,3) -- (7,14/3);
\draw[red, fill=white] (6,-1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-7,
xmax=3,
ymin=-5,
ymax=5,
]
\draw[blue, <->] (-7,-1) -- (-2,4) -- (3,-1);
\draw[red, fill=white] (2,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{4\baselineskip}
\end{document}
EOL
pdflatex Trans.22.tex
mv -f Trans.22.log ../log
mv -f Trans.22.tex ../tex
mv -f Trans.22.pdf ../pdf
	
rm Trans.23.tex
cat >Trans.23.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Write the vertex-form equation for each graph.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\draw[blue, <->] (-5,1) -- (0,3) -- (5,1);
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-8,
ymax=2,
]
\draw[blue, <->] (-3,-8) -- (-1,-2) -- (1, -8);
\draw[red, fill=white] (4,-7) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-2,
ymax=8,
]
\draw[blue, <->] (-5,3) -- (-2,1) -- (5,5.5);
\draw[red, fill=white] (4,-1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

\vspace{4\baselineskip}
\end{document}
EOL
pdflatex Trans.23.tex
mv -f Trans.23.log ../log
mv -f Trans.23.tex ../tex
mv -f Trans.23.pdf ../pdf
	
rm Trans.24.tex
cat >Trans.24.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Graph the equation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-1,
ymax=7,
]
\draw[red, fill=white] (6,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-1,
ymax=7,
]
\draw[red, fill=white] (6,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=7,
ymin=-1,
ymax=7,
]
\draw[red, fill=white] (6,1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

$y = \frac{2}{3}\abs{x - 3} + 4$
\hfill
$y = -\frac{4}{3}\abs{x - 2} + 6$
\hfill
$y = -\frac{3}{4}\abs{x - 3} + 6$

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.24.tex
mv -f Trans.24.log ../log
mv -f Trans.24.tex ../tex
mv -f Trans.24.pdf ../pdf
	
rm Trans.25.tex
cat >Trans.25.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Graph the function.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\draw[red, fill=white] (4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-2,
ymax=8,
]
\draw[red, fill=white] (4,7) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\draw[red, fill=white] (4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

$f(x) = \frac{2}{3}\abs{x - 1} - 2$
\hfill
$p(x) = \frac{3}{2}\abs{x + 1} + 2$
\hfill
$w(x) = -\frac{1}{3}\abs{x - 2} + 1$
\end{document}
EOL
pdflatex Trans.25.tex
mv -f Trans.25.log ../log
mv -f Trans.25.tex ../tex
mv -f Trans.25.pdf ../pdf
	
rm Trans.26.tex
cat >Trans.26.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Graph the function.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\draw[red, fill=white] (4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\draw[red, fill=white] (4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\draw[red, fill=white] (4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

$g(x) = -\frac{1}{4}\abs{x + 1} + 3$
\hfill
$q(x) = \frac{5}{2}\abs{x - 3} - 4$
\hfill
$m(x) = -\abs{x + 2} + 4$

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.26.tex
mv -f Trans.26.log ../log
mv -f Trans.26.tex ../tex
mv -f Trans.26.pdf ../pdf
	
rm Trans.27.tex
cat >Trans.27.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Using the graph, find all solutions to the equation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{-abs(x) + 4} node[above right, pos=.6, fill=white] {$g(x)$};
\addplot[red, samples=51, domain=-5:5]{abs(x - 1) - 1} node[below, pos=.6, fill=white] {$f(x)$};
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[blue, samples=51, domain=-5:5]{-0.5*abs(x) + 3} node[above right, pos=.5, fill=white] {$f(x)$};
\addplot[red, samples=51, domain=-3:3]{2*abs(x) - 2} node[below right, pos=.5, fill=white] {$p(x)$};
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-4,
xmax=6,
ymin=-2,
ymax=4,
]
\addplot[blue, samples=51, domain=-4:6]{-0.5*abs(x) + 3} node[above right, pos=.5, fill=white] {$w(x)$};
\addplot[red, samples=51, domain=-4:6]{-(1/6)*abs(x+2)+2)} node[below, yshift=-.5ex, pos=.5, fill=white] {$q(x)$};
\draw[red, fill=white] (5,-1) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

$g(x) = f(x)$
\hfill
$p(x) = f(x)$
\hfill
$q(x) = w(x)$

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.27.tex
mv -f Trans.27.log ../log
mv -f Trans.27.tex ../tex
mv -f Trans.27.pdf ../pdf
	
rm Trans.28.tex
cat >Trans.28.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Using the graph, find all solutions to the equation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[blue, samples=51, domain=-5:5]{-(1/6)*abs(x) - 3} node[above right, pos=.6, fill=white] {$k(x)$};
\addplot[red, samples=51, domain=-5:5]{(2/3)*abs(x - 1)} node[above, yshift=1.2ex, pos=.6, fill=white] {$s(x)$};
\draw[red, fill=white] (4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[blue, samples=51, domain=-5:5]{-0.75*abs(x + 2) + 2} node[above, yshift=1.5ex, pos=.1, fill=white] {$c(x)$};
\addplot[red, samples=51, domain=-3.5:3.5]{2*abs(x) - 2} node[below right, pos=.5, fill=white] {$j(x)$};
\draw[red, fill=white] (4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-1,
xmax=9,
ymin=-5,
ymax=5,
]
\addplot[blue, samples=51, domain=-1:9]{0.5*abs(x - 3) + 2} node[above left, pos=.5, fill=white] {$f(x)$};
\addplot[red, samples=51, domain=4:9]{-3*abs(x - 7) + 4)} node[right, xshift=.2em, pos=.2, fill=white] {$T(x)$};
\draw[red, fill=white] (8,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

$s(x) = k(x)$
\hfill
$c(x) = j(x)$
\hfill
$f(x) = T(x)$
\end{document}
EOL
pdflatex Trans.28.tex
mv -f Trans.28.log ../log
mv -f Trans.28.tex ../tex
mv -f Trans.28.pdf ../pdf
	
rm Trans.29.tex
cat >Trans.29.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Using the graph, find all solutions to the equation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-4,
xmax=6,
ymin=-5,
ymax=5,
]
\addplot[blue, samples=51, domain=-3:5]{abs(x - 1) + 1} node[below right, pos=.6, fill=white] {$g(x)$};
\draw[<->, red] (-4,4) -- (6,4);
\draw[red, fill=white] (5,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:2.5]{2*abs(x + 2) - 4} node[above right, yshift=1.5ex, pos=.1, fill=white] {$V(x)$};
\draw[<->, red] (-5,-2) -- (5,-2);
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-2,
xmax=8,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=2/3:16/3]{-3*abs(x - 3) + 2} node[right, xshift=.5ex, pos=.5, fill=white] {$z(x)$};
\draw[<->, red] (-2,4) -- (8,4);
\draw[red, fill=white] (7,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

$g(x) = 4$
\hfill
$-2 = V(x)$
\hfill
$4 = z(x)$

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.29.tex
mv -f Trans.29.log ../log
mv -f Trans.29.tex ../tex
mv -f Trans.29.pdf ../pdf
	
rm Trans.3.tex
cat >Trans.3.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Evaluate each expression.

\begin{tasks}(5)
\task $\abs{-9}$
\task $-\abs{5}$
\task $\abs{5 - 9}$
\task $5 - \abs{9}$
\task $\abs{9 - 5}$
\end{tasks}
\end{document}
EOL
pdflatex Trans.3.tex
mv -f Trans.3.log ../log
mv -f Trans.3.tex ../tex
mv -f Trans.3.pdf ../pdf
	
rm Trans.30.tex
cat >Trans.30.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Using the graph, find all solutions to the equation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-4,
xmax=6,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-4:6]{-abs(x + 1) + 3} node[above, pos=.3, fill=white] {$f(x)$};
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{(2/3)*abs(x) - 2} node[above right, yshift=1.5ex, pos=.1, fill=white] {$Q(x)$};
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-2,
xmax=8,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=0:6]{abs(x - 3) + 2} node[below, pos=.5, fill=white] {$G(x)$};
\draw[red, fill=white] (7,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

$1 = f(x)$
\hfill
$0 = Q(x)$
\hfill
$G(x) = 2$

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.30.tex
mv -f Trans.30.log ../log
mv -f Trans.30.tex ../tex
mv -f Trans.30.pdf ../pdf
	
rm Trans.31.tex
cat >Trans.31.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Using the graph, find all solutions to the equation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-6,
xmax=4,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-6:4]{-abs(x + 1) + 4} node[above left, pos=.4, fill=white] {$f(x)$};
\addplot[red, samples=51, domain=-6:4]{abs(x + 1) - 2} node[below left, pos=.4, fill=white] {$h(x)$};
\draw[red, fill=white] (3,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{(2/3)*abs(x) - 4} node[below left, pos=.2, fill=white] {$b(x)$};
\addplot[red, samples=51, domain=-5:5]{-(2/3)*(x - 3) - 2} node[above right, pos=.2, fill=white] {$p(x)$};
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-2,
xmax=8,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-1:7]{abs(x - 3) + 1} node[below right, pos=.6, fill=white] {$G(x)$};
\draw[<->, red] (-2,4) -- (8,4);
\draw[red, fill=white] (7,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}

$h(x) = 2$
\hfill
$-2 = b(x)$
\hfill
$G(x) = 0$

\vspace{2\baselineskip}
\end{document}
EOL
pdflatex Trans.31.tex
mv -f Trans.31.log ../log
mv -f Trans.31.tex ../tex
mv -f Trans.31.pdf ../pdf
	
rm Trans.32.tex
cat >Trans.32.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find all solutions to each equation. 

\begin{tasks}[after-skip=6\baselineskip](4)
\task $\abs{t} + 3 = 8$
\task $\abs{p} - 7 = 20$
\task $22 = 3\abs{b} + 1$
\task $-4\abs{c} - 5 = 31$
\end{tasks}
\end{document}
EOL
pdflatex Trans.32.tex
mv -f Trans.32.log ../log
mv -f Trans.32.tex ../tex
mv -f Trans.32.pdf ../pdf
	
rm Trans.33.tex
cat >Trans.33.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find all solutions to each equation. 

\begin{tasks}[after-skip=6\baselineskip](3)
\task $11 = \abs{x + 5} - 4$
\task $\abs{3 + t} - 2 = -10$
\task $7 = 2\abs{a - 6} - 9$
\end{tasks}
\end{document}
EOL
pdflatex Trans.33.tex
mv -f Trans.33.log ../log
mv -f Trans.33.tex ../tex
mv -f Trans.33.pdf ../pdf
	
rm Trans.34.tex
cat >Trans.34.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find all solutions to each equation. 

\begin{tasks}[after-skip=6\baselineskip](3)
\task $5\abs{k - 3} + 8 = -22$
\task $17 = \frac{3}{2}\abs{x + 5} + 8$
\task $-\frac{4}{5}\abs{y + 2} - 7 = 1$
\end{tasks}
\end{document}
EOL
pdflatex Trans.34.tex
mv -f Trans.34.log ../log
mv -f Trans.34.tex ../tex
mv -f Trans.34.pdf ../pdf
	
rm Trans.35.tex
cat >Trans.35.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find all solutions to each equation. 

\begin{tasks}[after-skip=6\baselineskip](3)
\task $0.2\abs{4 + c} - 2 = 3$
\task $-\frac{2}{7} \abs{x - 9} = 6$
\task $-13 = -\abs{t + 2} - 9$
\end{tasks}
\end{document}
EOL
pdflatex Trans.35.tex
mv -f Trans.35.log ../log
mv -f Trans.35.tex ../tex
mv -f Trans.35.pdf ../pdf
	
rm Trans.36.tex
cat >Trans.36.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each pair of triangles that shows a vertical translation.

\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
grid=both,
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,3) -- (5,3) -- (2,7) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$A$};
\draw[red] (8,3) -- (14,3) -- (8,11) -- cycle node[fill=white, inner sep=.3pt] at (10,6) {$B$};
\draw[ForestGreen] (2,13) -- (5,13) -- (2,17) -- cycle node[fill=white, inner sep=.3pt] at (2.7,14.5) {$C$};
\draw[orange] (10,11) -- (16,11) -- (10,19) -- cycle node[fill=white, inner sep=.3pt] at (12,14) {$D$};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,3) -- (5,3) -- (2,7) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$A$};
\draw[red] (8,3) -- (11,3) -- (11,7) -- cycle node[fill=white, inner sep=.3pt] at (10.2,4.5) {$B$};
\draw[ForestGreen] (8,13) -- (11,13) -- (11,17) -- cycle node[fill=white, inner sep=.3pt] at (10.2,14.5) {$C$};
\draw[orange] (2,7) -- (5,7) -- (2,17) -- cycle node[fill=white, inner sep=.3pt] at (3,10) {$D$};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.36.tex
mv -f Trans.36.log ../log
mv -f Trans.36.tex ../tex
mv -f Trans.36.pdf ../pdf
	
rm Trans.37.tex
cat >Trans.37.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each pair of triangles that shows a vertical translation.

\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,13) -- (5,13) -- (2,17) -- cycle node[fill=white, inner sep=.3pt] at (3,14.5) {$A$};
\draw[orange] (2,3) -- (5,7) -- (2,7) -- cycle node[fill=white, inner sep=.3pt] at (3,5.5) {$D$};
\draw[red] (8,3) -- (14,3) -- (14,7) -- cycle node[fill=white, inner sep=.3pt] at (12.2,4.5) {$B$};
\draw[ForestGreen] (8,13) -- (14,13) -- (14,17) -- cycle node[fill=white, inner sep=.3pt] at (12.2,14.5) {$C$};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,3) -- (5,3) -- (2,7) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$A$};
\draw[red] (2,12) -- (5,12) -- (2,16) -- cycle node[fill=white, inner sep=.3pt] at (3,13.2) {$B$};
\draw[ForestGreen] (12,11.8) -- (15,11.8) -- (12,8) -- cycle node[fill=white, inner sep=.3pt] at (13,10.5) {$C$};
\draw[orange] (12,12.2) -- (15,12.2) -- (12,16) -- cycle node[fill=white, inner sep=.3pt] at (13,13.2) {$D$};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.37.tex
mv -f Trans.37.log ../log
mv -f Trans.37.tex ../tex
mv -f Trans.37.pdf ../pdf
	
rm Trans.38.tex
cat >Trans.38.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each pair of triangles that shows a vertical translation.

\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,3) -- (5,3) -- (2,7) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$A$};
\draw[red] (2,12) -- (5,12) -- (5,16) -- cycle node[fill=white, inner sep=.3pt] at (3.8,13.2) {$B$};
\draw[ForestGreen] (12,5) -- (15,5) -- (12,13) -- cycle node[fill=white, inner sep=.3pt] at (13,7.5) {$C$};
\draw[orange] (12,11) -- (15,11) -- (12,19) -- cycle node[fill=white, inner sep=.3pt] at (13,13.2) {$D$};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,3) -- (8,3) -- (8,7) -- cycle node[fill=white, inner sep=.3pt] at (5.8,4.5) {$A$};
\draw[red] (2,7) -- (8,7) -- (8,11) -- cycle node[fill=white, inner sep=.3pt] at (5.8,8.5) {$B$};
\draw[ForestGreen] (12,10.8) -- (20,10.8) -- (12,8) -- cycle node[fill=white, inner sep=.3pt] at (14,9.5) {$C$};
\draw[orange] (12,11.2) -- (15,11.2) -- (12,19) -- cycle node[fill=white, inner sep=.3pt] at (13,13.2) {$D$};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.38.tex
mv -f Trans.38.log ../log
mv -f Trans.38.tex ../tex
mv -f Trans.38.pdf ../pdf
	
rm Trans.39.tex
cat >Trans.39.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each pair of triangles that shows a horizontal translation.

\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,5) -- (8,5) -- (8,9) -- cycle node[fill=white, inner sep=.3pt] at (6,6.5) {$A$};
\draw[red] (2,10) -- (8,10) -- (8,14) -- cycle node[fill=white, inner sep=.3pt] at (6,11.5) {$B$};
\draw[ForestGreen] (9,8) -- (12,8) -- (12,15) -- cycle node[fill=white, inner sep=.3pt] at (11,10) {$C$};
\draw[orange] (16,8) -- (19,8) -- (19,15) -- cycle node[fill=white, inner sep=.3pt] at (18,10) {$D$};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,3) -- (5,3) -- (2,11) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$A$};
\draw[red] (8,3) -- (11,3) -- (8,11) -- cycle node[fill=white, inner sep=.3pt] at (9.2,4.5) {$B$};
\draw[ForestGreen] (3,13) -- (10,13) -- (10,17) -- cycle node[fill=white, inner sep=.3pt] at (8,14.5) {$C$};
\draw[orange] (12,7) -- (19,7) -- (19,11) -- cycle node[fill=white, inner sep=.3pt] at (17,8.5) {$D$};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.39.tex
mv -f Trans.39.log ../log
mv -f Trans.39.tex ../tex
mv -f Trans.39.pdf ../pdf
	
rm Trans.4.tex
cat >Trans.4.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Evaluate each expression.

\begin{tasks}(5)
\task $\abs{-3} + 7$
\task $\abs{3} - 7$
\task $\abs{-3 + 7}$
\task $\abs{-3} - \abs{7}$
\task $\abs{-3 - (-7)}$
\end{tasks}
\end{document}
EOL
pdflatex Trans.4.tex
mv -f Trans.4.log ../log
mv -f Trans.4.tex ../tex
mv -f Trans.4.pdf ../pdf
	
rm Trans.40.tex
cat >Trans.40.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each pair of triangles that shows a horizontal translation.

\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,4) -- (10,4) -- (10,13) -- cycle node[fill=white, inner sep=.3pt] at (8,7.5) {$A$};
\draw[red] (1,13) -- (1,19) -- (4,19) -- cycle node[fill=white, inner sep=.3pt] at (2,17) {$B$};
\draw[ForestGreen] (4,13) -- (4,19) -- (7,19) -- cycle node[fill=white, inner sep=.3pt] at (5,17) {$C$};
\draw[orange] (11,5) -- (19,5) -- (19,14) -- cycle node[fill=white, inner sep=.3pt] at (17,7.5) {$D$};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,3) -- (5,3) -- (2,11) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$A$};
\draw[red] (10,14) -- (18,14) -- (18,17) -- cycle node[fill=white, inner sep=.3pt] at (16,15) {$B$};
\draw[ForestGreen] (2,14) -- (10,14) -- (10,17) -- cycle node[fill=white, inner sep=.3pt] at (8,15) {$C$};
\draw[orange] (2,11) -- (10,11) -- (10,14) -- cycle node[fill=white, inner sep=.3pt] at (8,12) {$D$};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.40.tex
mv -f Trans.40.log ../log
mv -f Trans.40.tex ../tex
mv -f Trans.40.pdf ../pdf
	
rm Trans.41.tex
cat >Trans.41.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each pair of triangles that shows a horizontal translation.

\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (1,5) -- (1,15) -- (8,15) -- cycle node[fill=white, inner sep=.3pt] at (2,10) {$A$};
\draw[orange] (4,5) -- (4,15) -- (11,15) -- cycle node[fill=white, inner sep=.3pt] at (6,10) {$D$};
\draw[ForestGreen] (12,5) -- (12,15) -- (19,15) -- cycle node[fill=white, inner sep=.3pt] at (14,13) {$C$};
\draw[red] (12,1) -- (12,11) -- (19,11) -- cycle node[fill=white, inner sep=.3pt] at (14,5.5) {$B$};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=1]
\begin{axis}[
axis line style={-Latex[round]},
xmin=0,
xmax=20,
ymin=0,
ymax=20,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (2,3) -- (5,3) -- (2,11) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$A$};
\draw[red] (8,3) -- (11,3) -- (8,10) -- cycle node[fill=white, inner sep=.3pt] at (9,4.5) {$B$};
\draw[ForestGreen] (10,14) -- (18,14) -- (18,18) -- cycle node[fill=white, inner sep=.3pt] at (16,15) {$C$};
\draw[orange] (12,3) -- (15,3) -- (12,11) -- cycle node[fill=white, inner sep=.3pt] at (13,4.5) {$D$};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.41.tex
mv -f Trans.41.log ../log
mv -f Trans.41.tex ../tex
mv -f Trans.41.pdf ../pdf
	
rm Trans.42.tex
cat >Trans.42.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Describe each translation.

\begin{minipage}{.33\textwidth}
\begin{tikzpicture}[scale=.75]
\begin{axis}[
xmin=-7,
xmax=7,
ymin=-7,
ymax=7,
xticklabel={\empty},
yticklabel={\empty},
]
\filldraw[blue] (4,2) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{A}$};
\filldraw[red] (4,6) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{B}$};
\filldraw[ForestGreen] (-3,2) circle (3pt) node[left, xshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{C}$};
\filldraw[orange] (4,-5) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{D}$};
\end{axis}
\end{tikzpicture}

\begin{tasks}[after-item-skip=\baselineskip, after-skip=\baselineskip, label=\fbox{\strut\makebox[\widthof{m}][c]{\arabic*}}]
\task $A$ to $B$
\task $A$ to $C$
\task $A$ to $D$
\end{tasks}
\end{minipage}
\begin{minipage}{.33\textwidth}
\begin{tikzpicture}[scale=.75]
\begin{axis}[
xmin=-7,
xmax=7,
ymin=-7,
ymax=7,
xticklabel={\empty},
yticklabel={\empty},
]
\filldraw[blue] (-3,-5) circle (3pt) node[below, yshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{A}$};
\filldraw[red] (-3,4) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{B}$};
\filldraw[ForestGreen] (-1,-5) circle (3pt) node[below, yshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{C}$};
\filldraw[orange] (5,-5) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{D}$};
\end{axis}
\end{tikzpicture}

\begin{tasks}[after-item-skip=\baselineskip, after-skip=\baselineskip, resume, label=\fbox{\strut\makebox[\widthof{m}][c]{\arabic*}}]
\task $A$ to $B$
\task $A$ to $C$
\task $A$ to $D$
\end{tasks}
\end{minipage}
\begin{minipage}{.33\textwidth}
\begin{tikzpicture}[scale=.75]
\begin{axis}[
xmin=-7,
xmax=7,
ymin=-7,
ymax=7,
xticklabel={\empty},
yticklabel={\empty},
]
\filldraw[blue] (3,-1) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{A}$};
\filldraw[red] (3,-5) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{B}$};
\filldraw[ForestGreen] (0,-1) circle (3pt) node[below left, xshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{C}$};
\filldraw[orange] (3,4) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{D}$};
\filldraw[violet] (-3,-1) circle (3pt) node[below left, xshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{E}$};
\end{axis}
\end{tikzpicture}

\begin{tasks}[after-item-skip=\baselineskip, after-skip=\baselineskip, resume, label=\fbox{\strut\makebox[\widthof{m}][c]{\arabic*}}]
\task $A$ to $E$
\task $B$ to $D$
\task $E$ to $C$
\end{tasks}
\end{minipage}
\end{document}
EOL
pdflatex Trans.42.tex
mv -f Trans.42.log ../log
mv -f Trans.42.tex ../tex
mv -f Trans.42.pdf ../pdf
	
rm Trans.43.tex
cat >Trans.43.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Draw each transformation.

\begin{minipage}{.33\textwidth}
\begin{tikzpicture}[scale=.75]
\begin{axis}[
xmin=-7,
xmax=7,
ymin=-7,
ymax=7,
xticklabel={\empty},
yticklabel={\empty},
]
\filldraw[blue] (3,5) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{A}$};
\filldraw[red] (-2,3) circle (3pt) node[below, yshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{B}$};
\filldraw[ForestGreen] (0,-4) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{C}$};
\end{axis}
\end{tikzpicture}

\begin{tasks}[after-item-skip=\baselineskip, after-skip=\baselineskip, label=\fbox{\strut\makebox[\widthof{m}][c]{\arabic*}}]
\task translate $A$ down $4$
\task translate $B$ right $2$
\task translate $C$ left $4$
\end{tasks}
\end{minipage}
\begin{minipage}{.33\textwidth}
\begin{tikzpicture}[scale=.75]
\begin{axis}[
xmin=-7,
xmax=7,
ymin=-7,
ymax=7,
xticklabel={\empty},
yticklabel={\empty},
]
\filldraw[blue] (-3,-2) circle (3pt) node[below, yshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{D}$};
\filldraw[red] (3,0) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{E}$};
\filldraw[ForestGreen] (5,-5) circle (3pt) node[below, yshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{F}$};
\end{axis}
\end{tikzpicture}

\begin{tasks}[after-item-skip=\baselineskip, after-skip=\baselineskip, resume, label=\fbox{\strut\makebox[\widthof{m}][c]{\arabic*}}]
\task translate $D$ left $3$
\task translate $E$ down $5$
\task translate $E$ up $5$
\end{tasks}
\end{minipage}
\begin{minipage}{.33\textwidth}
\begin{tikzpicture}[scale=.75]
\begin{axis}[
xmin=-7,
xmax=7,
ymin=-7,
ymax=7,
xticklabel={\empty},
yticklabel={\empty},
]
\filldraw[blue] (3,-1) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{A}$};
\filldraw[red] (3,-5) circle (3pt) node[right, xshift=1ex, inner sep=0ex, fill=white] {$\mathbold{B}$};
\filldraw[ForestGreen] (0,-1) circle (3pt) node[below left, xshift=-1ex, inner sep=0ex, fill=white] {$\mathbold{C}$};
\end{axis}
\end{tikzpicture}

\begin{tasks}[after-item-skip=\baselineskip, after-skip=\baselineskip, resume, label=\fbox{\strut\makebox[\widthof{m}][c]{\arabic*}}]
\task translate $B$ up $4$
\task translate $C$ down $4$
\task translate $C$ up $1$
\end{tasks}
\end{minipage}
\end{document}
EOL
pdflatex Trans.43.tex
mv -f Trans.43.log ../log
mv -f Trans.43.tex ../tex
mv -f Trans.43.pdf ../pdf
	
rm Trans.44.tex
cat >Trans.44.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find the coordinates of the new point.

\begin{tasks}[after-item-skip=4\baselineskip, after-skip=4\baselineskip](2)
\task vertical translation of $(5,-2)$ by $8$ units
\task horizontal translation of $(4,9)$ by $-1$ units
\task horizontal translation of $(-7,3)$ by $5$ units
\task vertical translation of $(-2,8)$ by $-11$ units
\end{tasks}
\end{document}
EOL
pdflatex Trans.44.tex
mv -f Trans.44.log ../log
mv -f Trans.44.tex ../tex
mv -f Trans.44.pdf ../pdf
	
rm Trans.45.tex
cat >Trans.45.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find the coordinates of the new point.

\begin{tasks}[after-item-skip=4\baselineskip, after-skip=4\baselineskip](2)
\task horizontal translation of $(0,5)$ by $-8$ units
\task vertical translation of $(0,-3)$ by $-5$ units
\task horizontal translation of $(-4,0)$ by $5$ units
\task vertical translation of $(6,-7)$ by $10$ units
\end{tasks}
\end{document}
EOL
pdflatex Trans.45.tex
mv -f Trans.45.log ../log
mv -f Trans.45.tex ../tex
mv -f Trans.45.pdf ../pdf
	
rm Trans.46.tex
cat >Trans.46.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each part that represents a vertical translation of the parent function $y = g(x)$.
Then describe the translation.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](4)
\task $y = g(x - 3)$
\task $y = g(x) - 7$
\task $y = 5 + g(x)$
\task $y = g(x + 8)$
\task $y = g(x) + 1$
\task $y = 7g(x)$
\task $y = 7g(x) + 2$
\task $y = g(3x)$
\end{tasks}
\end{document}
EOL
pdflatex Trans.46.tex
mv -f Trans.46.log ../log
mv -f Trans.46.tex ../tex
mv -f Trans.46.pdf ../pdf
	
rm Trans.47.tex
cat >Trans.47.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each part that represents a vertical translation of the parent function $y = p(x)$.
Then describe the translation.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](4)
\task $y = p(x) - 4$
\task $y = 4 - p(x)$
\task $y = 5 + p(x)$
\task $y = 5 + 3p(x)$
\task $y = 3p(x + 5)$
\task $y = p(x + 5)$
\task $y = p(x) + 5$
\task $y - 5 = p(x)$
\end{tasks}
\end{document}
EOL
pdflatex Trans.47.tex
mv -f Trans.47.log ../log
mv -f Trans.47.tex ../tex
mv -f Trans.47.pdf ../pdf
	
rm Trans.48.tex
cat >Trans.48.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
The graph of $y = g(x)$ is a transformation of the parent function $y = f(x)$. 
Express $g(x)$ in terms of $f(x)$ using function notation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-2:4]{abs(x - 1) + 2} node[below right, pos=.6, fill=white] {$y = f(x)$}; 
\addplot[samples=51, red, domain=-5:5]{abs(x - 1) - 3} node[below right, pos=.6, fill=white] {$y = g(x)$};
\draw[red, fill=white] (-4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{abs(x + 2) - 4} node[below right, pos=.4, fill=white] {$y = f(x)$}; 
\addplot[samples=51, red, domain=-5:4]{abs(x + 2) - 1} node[above left, pos=.7, fill=white] {$y = g(x)$};
\draw[red, fill=white] (-4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{-abs(x + 1) + 3} node[above right, pos=.5, fill=white] {$y = f(x)$}; 
\addplot[samples=51, red, domain=-5:3]{-abs(x + 1) - 1} node[below right, pos=.2, fill=white] {$y = g(x)$};
\draw[red, fill=white] (-4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.48.tex
mv -f Trans.48.log ../log
mv -f Trans.48.tex ../tex
mv -f Trans.48.pdf ../pdf
	
rm Trans.49.tex
cat >Trans.49.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
The graph of $y = g(x)$ is a transformation of the parent function $y = f(x)$. 
Express $g(x)$ in terms of $f(x)$ using function notation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{-abs(x - 3) + 4} node[above left, pos=.6, fill=white] {$y = f(x)$}; 
\addplot[samples=51, red, domain=-3:5]{-abs(x - 3) + 1} node[below right, pos=.5, fill=white] {$y = g(x)$};
\draw[red, fill=white] (-4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-2:2]{abs(x) + 3} node[below right, pos=.5, fill=white] {$y = f(x)$}; 
\addplot[samples=51, red, domain=-5:5]{abs(x) - 3} node[below right, pos=.5, fill=white] {$y = g(x)$};
\draw[red, fill=white] (-4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{-abs(x - 1) + 2} node[above right, pos=.6, fill=white] {$y = f(x)$}; 
\addplot[samples=51, red, domain=-4:5]{-abs(x - 1)} node[below, pos=.6, yshift=-4ex, fill=white] {$y = g(x)$};
\draw[red, fill=white] (-4,4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.49.tex
mv -f Trans.49.log ../log
mv -f Trans.49.tex ../tex
mv -f Trans.49.pdf ../pdf
	
rm Trans.5.tex
cat >Trans.5.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Evaluate each expression.

\begin{tasks}[after-skip=4\baselineskip](5)
\task $3\abs{12 - 7} - 6$
\task $3\abs{2 - 7} - 6$
\task $-5\abs{4 - 7} - 6$
\task $-5(4 - 7) - 6$
\task $-5\abs{4} - 7 - 6$
\end{tasks}
\end{document}
EOL
pdflatex Trans.5.tex
mv -f Trans.5.log ../log
mv -f Trans.5.tex ../tex
mv -f Trans.5.pdf ../pdf
	
rm Trans.50.tex
cat >Trans.50.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each part that represents a horizontal translation of the parent function $y = q(x)$.
Then describe the translation.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](4)
\task $y = q(x - 2)$
\task $y = q(x) + 7$
\task $y = 7 + q(x)$
\task $y = q(x + 8)$
\task $y = q(8 + x)$
\task $y = 8q(x)$
\task $y = 8q(x + 2)$
\task $y = q(3 - x)$
\end{tasks}
\end{document}
EOL
pdflatex Trans.50.tex
mv -f Trans.50.log ../log
mv -f Trans.50.tex ../tex
mv -f Trans.50.pdf ../pdf
	
rm Trans.51.tex
cat >Trans.51.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each part that represents a horizontal translation of the parent function $y = w(x)$.
Then describe the translation.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](4)
\task $y = w(x) - 4$
\task $y = w(4 - x)$
\task $y = w(x - 4)$
\task $y = 7 - w(x)$
\task $y = w(x + 5)$
\task $y = w(5x + 1)$
\task $y = w(x) + 5$
\task $y - 5 = w(x - 2)$
\end{tasks}
\end{document}
EOL
pdflatex Trans.51.tex
mv -f Trans.51.log ../log
mv -f Trans.51.tex ../tex
mv -f Trans.51.pdf ../pdf
	
rm Trans.52.tex
cat >Trans.52.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
The graph of $y = g(x)$ is a transformation of the parent function $y = f(x)$. 
Express $g(x)$ in terms of $f(x)$ using function notation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{-abs(x - 3) + 3} node[above, pos=.8, fill=white] {$y = f(x)$}; 
\addplot[dashed, samples=51, red, domain=-5:5]{-abs(x + 1) + 3} node[above left, pos=.4, fill=white] {$y = g(x)$};
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:0]{abs(x + 3) + 2} node[below, pos=.3, yshift=-2ex, fill=white] {$y = f(x)$}; 
\addplot[dashed, samples=51, red, domain=-1:5]{abs(x - 2) + 2} node[below right, pos=.5, fill=white] {$y = g(x)$};
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-2:5]{abs(x - 3)} node[below left, pos=.1, fill=white] {$y = f(x)$}; 
\addplot[dashed, samples=51, red, domain=-1:5]{abs(x - 4)} node[right, pos=.2, xshift=2ex, fill=white] {$y = g(x)$};
\draw[red, fill=white] (4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.52.tex
mv -f Trans.52.log ../log
mv -f Trans.52.tex ../tex
mv -f Trans.52.pdf ../pdf
	
rm Trans.53.tex
cat >Trans.53.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
The graph of $y = q(x)$ is a transformation of the parent function $y = p(x)$. 
Express $q(x)$ in terms of $p(x)$ using function notation.

\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=1:5]{abs(x - 3) + 3} node[below left, pos=.5, fill=white] {$y = p(x)$}; 
\addplot[dashed, samples=51, red, domain=-3:1]{abs(x + 1) + 3} node[below left, pos=.5, fill=white] {$y = q(x)$};
\draw[red, fill=white] (-4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:3]{abs(x + 2)} node[below, pos=.3, yshift=-4ex, fill=white] {$y = p(x)$}; 
\addplot[dashed, samples=51, red, domain=-3:5]{abs(x - 2)} node[below, pos=.7, yshift=-4ex, fill=white] {$y = q(x)$};
\draw[red, fill=white] (-4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=.7]
\begin{axis}[
xmin=-5,
xmax=5,
ymin=-5,
ymax=5,
]
\addplot[samples=51, domain=-5:5]{abs(x) - 4} node[below right, pos=.7, fill=white] {$y = p(x)$}; 
\addplot[dashed, samples=51, red, domain=-3:3]{abs(x) + 2} node[below right, pos=.7, fill=white] {$y = q(x)$};
\draw[red, fill=white] (-4,-4) circle (1em) node {\large \stask \normalsize};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.53.tex
mv -f Trans.53.log ../log
mv -f Trans.53.tex ../tex
mv -f Trans.53.pdf ../pdf
	
rm Trans.54.tex
cat >Trans.54.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Describe which transformations to apply to obtain the function $v(t)$ from the parent function $p(t) = \abs{t}$.

\begin{tasks}[after-item-skip=3\baselineskip, after-skip=2\baselineskip](3)
\task $v(t) = \abs{t + 3} + 5$
\task $v(t) = \abs{t - 2} - 6$
\task $v(t) = \abs{t + 7} - 3$
\task $v(t) = 8 + \abs{t + 4}$
\task $v(t) = \abs{t - 10}$
\end{tasks}
\end{document}
EOL
pdflatex Trans.54.tex
mv -f Trans.54.log ../log
mv -f Trans.54.tex ../tex
mv -f Trans.54.pdf ../pdf
	
rm Trans.55.tex
cat >Trans.55.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Write the vertex-form equation for the absolute-value function that results from applying the transformation to the original function.

\begin{tasks}[after-item-skip=3\baselineskip, after-skip=2\baselineskip](2)
\task translate $p(x) = 3\abs{x - 3} - 2$ up $7$ and left $4$
\task translate $q(x) = -8\abs{x + 1} - 5$ down $3$ and left $5$
\task translate $b(t) = -2\abs{t + 5} - 7$ down $4$ and right $8$
\task translate $g(x) = 9\abs{x + 7}$ up $7$ and right $3$ 
\end{tasks}
\end{document}
EOL
pdflatex Trans.55.tex
mv -f Trans.55.log ../log
mv -f Trans.55.tex ../tex
mv -f Trans.55.pdf ../pdf
	
rm Trans.56.tex
cat >Trans.56.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Write the vertex-form equation for the absolute-value function that results from applying the transformation to the original function.

\begin{tasks}[after-item-skip=3\baselineskip, after-skip=2\baselineskip](2)
\task translate $f(x) = -5\abs{x + 4} + 1$ down $8$ and right $3$
\task translate $P(x) = 6\abs{x - 3} + 9$ up $4$ and left $5$
\task translate $q(a) = -2\abs{a + 5} - 3$ up $1$ and right $4$
\task translate $m(t) = -\abs{t + 3} - 2$ down $5$ and left $3$
\end{tasks}
\end{document}
EOL
pdflatex Trans.56.tex
mv -f Trans.56.log ../log
mv -f Trans.56.tex ../tex
mv -f Trans.56.pdf ../pdf
	
rm Trans.57.tex
cat >Trans.57.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each pair of triangles that shows a reflection across the $x$-axis. 

\begin{tikzpicture}[scale=1]
\begin{axis}[
xmin=-10,
xmax=10,
ymin=-10,
ymax=10,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (-2,-2) -- (-5,-2) -- (-2,-9) -- cycle node[fill=white, inner sep=.3pt] at (-3,-4.5) {$A$};
\draw[red] (2,-2) -- (5,-2) -- (2,-9) -- cycle node[fill=white, inner sep=.3pt] at (3,-4.5) {$B$};
\draw[ForestGreen] (6,2) -- (9,2) -- (6,9) -- cycle node[fill=white, inner sep=.3pt] at (7,4.5) {$C$};
\draw[orange] (2,2) -- (5,2) -- (2,9) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$D$};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=1]
\begin{axis}[
xmin=-10,
xmax=10,
ymin=-10,
ymax=10,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (-2,-2) -- (-5,-2) -- (-5,-9) -- cycle node[fill=white, inner sep=.3pt] at (-4,-4.5) {$A$};
\draw[red] (-2,2) -- (-5,2) -- (-5,9) -- cycle node[fill=white, inner sep=.3pt] at (-4,4.5) {$B$};
\draw[ForestGreen] (6,2) -- (9,2) -- (6,9) -- cycle node[fill=white, inner sep=.3pt] at (7,4.5) {$C$};
\draw[orange] (2,2) -- (5,2) -- (5,9) -- cycle node[fill=white, inner sep=.3pt] at (4,4.5) {$D$};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.57.tex
mv -f Trans.57.log ../log
mv -f Trans.57.tex ../tex
mv -f Trans.57.pdf ../pdf
	
rm Trans.58.tex
cat >Trans.58.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each pair of triangles that shows a reflection across the $x$-axis. 

\begin{tikzpicture}[scale=1]
\begin{axis}[
xmin=-10,
xmax=10,
ymin=-10,
ymax=10,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (-2,2) -- (-2,5) -- (-9,5) -- cycle node[fill=white, inner sep=.3pt] at (-3,3.5) {$A$};
\draw[red] (-2,-2) -- (-9,-2) -- (-2,-5) -- cycle node[fill=white, inner sep=.3pt] at (-3,-3.5) {$B$};
\draw[ForestGreen] (6,2) -- (9,2) -- (6,5) -- cycle node[fill=white, inner sep=.3pt] at (7,3) {$C$};
\draw[orange] (6,6) -- (9,9) -- (6,9) -- cycle node[fill=white, inner sep=.3pt] at (7,8) {$D$};
\draw[violet] (6,-2) -- (9,-2) -- (6,-5) -- cycle node[fill=white, inner sep=.3pt] at (7,-3) {$E$};
\end{axis}
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=1]
\begin{axis}[
xmin=-10,
xmax=10,
ymin=-10,
ymax=10,
xticklabel={\empty},
yticklabel={\empty},
]
\draw[blue] (-2,2) -- (4,2) -- (4,9) -- cycle node[fill=white, inner sep=.3pt] at (3,4.5) {$A$};
\draw[red] (2,2) -- (-4,2) -- (-4,9) -- cycle node[fill=white, inner sep=.3pt] at (-3,4.5) {$B$};
\draw[ForestGreen] (5,2) -- (9,2) -- (9,-8) -- cycle node[fill=white, inner sep=.3pt] at (8,-3.5) {$C$};
\draw[orange] (5,-2) -- (9,-2) -- (9,8) -- cycle node[fill=white, inner sep=.3pt] at (8,3.5) {$D$};
\draw[violet] (2,-2) -- (-4,-2) -- (-4,-9) -- cycle node[fill=white, inner sep=.3pt] at (-3,-4.5) {$E$};
\end{axis}
\end{tikzpicture}
\end{document}
EOL
pdflatex Trans.58.tex
mv -f Trans.58.log ../log
mv -f Trans.58.tex ../tex
mv -f Trans.58.pdf ../pdf
	
rm Trans.59.tex
cat >Trans.59.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find the coordinates of the new point.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](2)
\task reflect $(3,-10)$ across the $x$-axis
\task reflect $(-4,7)$ across the $y$-axis
\task reflect $(-5,-8)$ across the $y$-axis
\task reflect $(7,3)$ across the $x$-axis
\end{tasks}
\end{document}
EOL
pdflatex Trans.59.tex
mv -f Trans.59.log ../log
mv -f Trans.59.tex ../tex
mv -f Trans.59.pdf ../pdf
	
rm Trans.6.tex
cat >Trans.6.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Evaluate each function at the given value.

\begin{tasks}[after-item-skip=6\baselineskip, after-skip=6\baselineskip](2)
\task $p(x) = \abs{x - 5} + 7$ \quad \fbox{$x = 3$}
\task $g(t) = 2\abs{t + 3} + 7$ \quad \fbox{$t = -8$}
\task $b(z) = -3\abs{z - 10} - 4$ \quad \fbox{$z = 8$}
\task $f(x) = -3\abs{x - 4} - 2$ \quad \fbox{$x = -5$}
\end{tasks}
\end{document}
EOL
pdflatex Trans.6.tex
mv -f Trans.6.log ../log
mv -f Trans.6.tex ../tex
mv -f Trans.6.pdf ../pdf
	
rm Trans.60.tex
cat >Trans.60.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Find the coordinates of the new point.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](2)
\task reflect $(5,-4)$ across the $x$-axis, then translate right $5$ units
\task translate $(-4,9)$ up $3$, then reflect across then $y$-axis
\task translate $(0,-5)$ up $3$, then reflect across the $x$-axis
\task reflect $(1,-3)$ across the $x$-axis, then translate left $2$
\end{tasks}
\end{document}
EOL
pdflatex Trans.60.tex
mv -f Trans.60.log ../log
mv -f Trans.60.tex ../tex
mv -f Trans.60.pdf ../pdf
	
rm Trans.61.tex
cat >Trans.61.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each part that represents a reflection of the parent function $y = R(x)$.
Then describe the reflection.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](4)
\task $y = R(x - 3)$
\task $y = R(-x)$
\task $y = 3 - R(x)$
\task $y = -R(x)$
\task $-y = R(x)$
\task $y - 1 = R(x)$
\task $-y = R(x) + 4$
\task $y = -f(x)$
\end{tasks}
\end{document}
EOL
pdflatex Trans.61.tex
mv -f Trans.61.log ../log
mv -f Trans.61.tex ../tex
mv -f Trans.61.pdf ../pdf
	
rm Trans.62.tex
cat >Trans.62.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Circle each part that represents a reflection of the parent function $y = h(t)$.
Then describe the reflection.

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](4)
\task $y = h(5 - t)$
\task $y = 5 - h(t)$
\task $y = -h(t)$
\task $\displaystyle y = \frac{1}{h(t)}$
\task $y = h(-t)$
\task $y = h(-(-t))$
\task $-y = h(t)$
\task $\displaystyle y = \frac{1}{2}h(t)$
\end{tasks}
\end{document}
EOL
pdflatex Trans.62.tex
mv -f Trans.62.log ../log
mv -f Trans.62.tex ../tex
mv -f Trans.62.pdf ../pdf
	
rm Trans.63.tex
cat >Trans.63.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Describe each transformation of the parent function $y = g(x)$ in terms of translations and reflections.
\textbf{Pay attention to the order of operations!}

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](3)
\task $y = -g(x - 3)$
\task $y = g(x + 4) - 1$
\task $y = g(-x) - 3$
\task $y = -g(x) + 5$
\task $y = -(g(x) + 3)$
\end{tasks}
\end{document}
EOL
pdflatex Trans.63.tex
mv -f Trans.63.log ../log
mv -f Trans.63.tex ../tex
mv -f Trans.63.pdf ../pdf
	
rm Trans.64.tex
cat >Trans.64.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Describe each transformation of the parent function $y = c(x)$ in terms of translations and reflections.
\textbf{Pay attention to the order of operations!}

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](3)
\task $y = c(-x) - 6$
\task $y = -c(x) + 7$
\task $y = -c(x - 3) + 2$
\task $-y = c(x - 4)$
\task $y = -c(-x) - 7$
\end{tasks}
\end{document}
EOL
pdflatex Trans.64.tex
mv -f Trans.64.log ../log
mv -f Trans.64.tex ../tex
mv -f Trans.64.pdf ../pdf
	
rm Trans.65.tex
cat >Trans.65.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Describe each transformation of the parent function $y = d(x)$ in terms of translations and reflections.
\textbf{Pay attention to the order of operations!}

\begin{tasks}[after-item-skip=2\baselineskip, after-skip=2\baselineskip](3)
\task $y = -d(-x)$
\task $y = -d(x) - 7$
\task $-y = d(x + 2) - 8$
\task $y = d(x - 3) - 6$
\task $y = -d(-x) + 1$
\end{tasks}
\end{document}
EOL
pdflatex Trans.65.tex
mv -f Trans.65.log ../log
mv -f Trans.65.tex ../tex
mv -f Trans.65.pdf ../pdf
	
rm Trans.66.tex
cat >Trans.66.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Write the vertex-form equation for the absolute-value function that results from applying the transformation to the original function.

\begin{tasks}[after-item-skip=3\baselineskip, after-skip=3\baselineskip](2)
\task reflect $q(x) = 3\abs{x - 5}$ across the $y$-axis, then translate right $4$
\task reflect $p(x) = -2\abs{x} + 3$ across the $x$-axis, then translate down $5$
\task translate $r(x) = 5\abs{x} - 1$ down $4$, then reflect across the $x$-axis
\task translate $t(x) = -\abs{x + 4}$ left $5$, then reflect across the $y$-axis
\end{tasks}
\end{document}
EOL
pdflatex Trans.66.tex
mv -f Trans.66.log ../log
mv -f Trans.66.tex ../tex
mv -f Trans.66.pdf ../pdf
	
rm Trans.67.tex
cat >Trans.67.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Write the vertex-form equation for the absolute-value function that results from applying the transformation to the original function.

\begin{tasks}[after-item-skip=3\baselineskip, after-skip=3\baselineskip](2)
\task reflect $p(x) = -8\abs{x} + 3$ across the $x$-axis, then translate up $2$
\task translate $r(x) = 5\abs{x+1} - 3$ down $4$, then translate left $10$
\task reflect $a(x) = \abs{x + 2}$ across the $y$-axis, then translate left $5$
\task translate $c(x) = -\abs{x + 4}$ right $1$, then reflect across the $y$-axis
\end{tasks}
\end{document}
EOL
pdflatex Trans.67.tex
mv -f Trans.67.log ../log
mv -f Trans.67.tex ../tex
mv -f Trans.67.pdf ../pdf
	
rm Trans.7.tex
cat >Trans.7.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Evaluate each function at the given value.

\begin{tasks}[after-item-skip=6\baselineskip, after-skip=6\baselineskip](2)
\task $q(t) = 5\abs{t - 3} - 6$ \quad \fbox{$t = 2$}
\task $h(t) = -2\abs{t + 1} - 8$ \quad \fbox{$t = -4$}
\task $m(x) = 8 - 3\abs{x + 2}$ \quad \fbox{$x = 8$}
\task $f(a) = 6 + \abs{4 - a}$ \quad \fbox{$a = -3$}
\end{tasks}
\end{document}
EOL
pdflatex Trans.7.tex
mv -f Trans.7.log ../log
mv -f Trans.7.tex ../tex
mv -f Trans.7.pdf ../pdf
	
rm Trans.8.tex
cat >Trans.8.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Evaluate each function at the given value.

\begin{tasks}[after-item-skip=6\baselineskip, after-skip=6\baselineskip](2)
\task $d(t) = \frac{2}{5}\abs{t - 3} + 1$ \quad \fbox{$t = -12$}
\task $h(t) = 10 - \frac{3}{2}\abs{t + 4}$ \quad \fbox{$t = 0$}
\task $r(x) = 1.7 - 0.2\abs{x + 3}$ \quad \fbox{$x = 0.1$}
\task $p(x) = 8.5 - \frac{1}{2}\abs{x - 7}$ \quad \fbox{$x = -2$}
\end{tasks}
\end{document}
EOL
pdflatex Trans.8.tex
mv -f Trans.8.log ../log
mv -f Trans.8.tex ../tex
mv -f Trans.8.pdf ../pdf
	
rm Trans.9.tex
cat >Trans.9.tex << \EOL
\documentclass[crop=false]{standalone}
\usepackage{../preamble-standalone}
\begin{document}
Evaluate each function at the given value.

\begin{tasks}[after-item-skip=6\baselineskip, after-skip=6\baselineskip](2)
\task $a(m) = -\frac{3}{7}\abs{m - 3} + 2$ \quad \fbox{$m = 24$}
\task $h(t) = -10(t - 12) + 8$ \quad \fbox{$t = 5$}
\task $q(x) = -3\abs{x + 1} - 4.1$ \quad \fbox{$x = 5.2$}
\task $q(x) = -\abs{x - 5} - 9$ \quad \fbox{$x = -4$}
\end{tasks}
\end{document}
EOL
pdflatex Trans.9.tex
mv -f Trans.9.log ../log
mv -f Trans.9.tex ../tex
mv -f Trans.9.pdf ../pdf
