% Math packages

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{etoolbox}
\usepackage{mathtools}
\usepackage{xfrac}

% Fonts

%\usepackage{cmbright, sfmath}
\usepackage{pxfonts}
\usepackage[euler-digits]{eulervm}
\usepackage{numprint}
\npthousandsep{\,}

%\everymath{\displaystyle}

%\usepackage[english]{isodate}
%\isodate

\usepackage[margin=.75in]{geometry}
\setlength{\marginparwidth}{0pt}
\setlength{\parindent}{0em}
\raggedright

% Header and footer

\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}

% Chapter and section names

\usepackage{titlesec}
\titleformat{\chapter}{\raggedleft\bfseries\sffamily\huge}{}{1em}{\thispagestyle{fancy}\fbox}
\titlespacing*{\chapter}{0pt}{*2}{*2}
\titleformat{\section}{\raggedleft\bfseries\sffamily\Large}{}{1em}{
  \titlerule[.4pt]%
  \fbox}
\titlespacing*{\section}{0pt}{*2}{*2}

% Table of contents

\renewcommand*\contentsname{}
\newcommand*{\setupTOC}{%
\titleformat{\chapter}{}{}{0pt}{}
\titlespacing*{\chapter}{0pt}{*7}{*-15}}

% Lists and arrays

\usepackage[inline]{enumitem}
\usepackage{array}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{systeme}
\usepackage[dvipsnames, table]{xcolor}

\usepackage{framed}

% Tikz

\usepackage{pgfplots}
\usetikzlibrary{arrows}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{intersections}
\usetikzlibrary{matrix}
\usetikzlibrary{patterns}
\usetikzlibrary{tikzmark}

\tikzset{
  >={Latex[round]},                                                       % arrow style
}

\pgfplotsset{
  compat=1.18,
  every axis/.append style={grid=both},                                   % show grid lines
  every axis/.append style={major grid style={line width=1pt}},           % grid line thickness
  every axis/.append style={major grid style={draw=blue!90}},             % grid line color
  every axis/.append style={major grid style={densely dotted}},           % grid line type 
  every axis/.append style={line width=2.5pt},                            % axis thickness
  every axis/.append style={gray!99},                                     % axis color
  every axis/.append style={axis lines=center},                           % axis alignment
  every axis/.append style={axis line style={Latex[round]-Latex[round]}}, % axis arrows
  every axis/.append style={label style={black!80}},                      % axis label color
  every axis/.append style={xlabel={$x$}},                                % x-axis label
  every axis/.append style={ylabel={$y$}},                                % y-axis label
  every axis/.append style={xtick={-20,...,20}},                          % x-ticks
  every axis/.append style={ytick={-20,...,20}},                          % y-ticks
  every x tick label/.append style={font=\boldmath},                      % x-tick font
  every y tick label/.append style={font=\boldmath},                      % y-tick font
  every x tick label/.append style={black},                               % x-tick color
  every y tick label/.append style={black},                               % y-tick color
  every x tick label/.append style={yshift=.7ex},                         % x-tick position
  every y tick label/.append style={xshift=.8ex},                         % y-tick position
  every axis plot/.append style={line width=2.5pt},                       % plot line thickness 
  every axis plot/.append style={blue},                                   % plot line color
  every axis plot/.append style={samples=2},                              % plot samples
  every axis plot/.append style={<->},                                    % plot arrows 
}

% Exercise environments

\usepackage{thmtools}
\theoremstyle{definition}
\newtheorem{exercise}{}[section]
\renewcommand{\theexercise}{\thesection.\arabic{exercise}}
\declaretheoremstyle[%
  headfont=\color{violet}\normalfont\bfseries,
  bodyfont=\color{violet}\normalfont,
]{wb}
\newenvironment{nobreaks}{\vbox\bgroup}{\egroup}
\AtBeginEnvironment{exercise}{\begin{nobreaks}}
\AtEndEnvironment{exercise}{\end{nobreaks}}

% Tasks 

\usepackage{tasks}
\settasks{
  label=\fbox{\strut\makebox[\widthof{m}][c]{\alph*}},
  label-width=2em, 
  label-offset=.3em,
  after-item-skip=4\baselineskip,
  after-skip=4\baselineskip}

\newcounter{specialtask}
\numberwithin{specialtask}{exercise}
\newcommand{\stask}{\addtocounter{specialtask}{1}\alph{specialtask}}

% Multiple choice

\NewTasksEnvironment[
  label=\textbf{\Alph*}, 
  after-skip=\itemsep, 
  after-item-skip=\itemsep]{mc}[\choice](1)

% Answer blanks

\newcommand{\blank}[1][1]{
  \ensuremath{
  \vcenter{\hbox{
  \begin{tikzpicture}[scale=.7]
    \filldraw[blue!12] (0,0) rectangle (#1,1);
  \end{tikzpicture}
}}}}
\newcommand{\blankfraction}[1][1]{\ensuremath{\frac{\blank[#1]}{\blank[#1]}}}
\newcommand{\wblank}[1][1]{
  \ensuremath{
  \vcenter{\hbox{
  \begin{tikzpicture}[scale=.7]
    \filldraw[white] (0,0) rectangle (#1,1);
  \end{tikzpicture}
}}}}
\newcommand{\wblankfraction}[1][1]{\ensuremath{\frac{\wblank[#1]}{\wblank[#1]}}}
\newcommand{\blankbox}[1][1]{
  \ensuremath{
  \vcenter{\hbox{
  \begin{tikzpicture}
    \draw[blue!80, line width=1pt] (0,0) rectangle (#1,1);
  \end{tikzpicture}
}}}}
\newcommand{\blankboxfraction}[1][1]{\ensuremath{\frac{\blankbox[#1]}{\blankbox[#1]}}}

% Special symbols

\newcommand{\slope}{\ensuremath{\frac{\Delta y}{\Delta x}}}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}

% Inequality symbols

\DeclareFontFamily{U}{matha}{\hyphenchar\font45}
\DeclareFontShape{U}{matha}{m}{n}{
      <5> <6> <7> <8> <9> <10> gen * matha
      <10.95> matha10 <12> <14.4> <17.28> <20.74> <24.88> matha12
      }{}
\DeclareSymbolFont{matha}{U}{matha}{m}{n}
\DeclareFontSubstitution{U}{matha}{m}{n}
\DeclareMathSymbol{\varleq}{3}{matha}{"A8}
\DeclareMathSymbol{\vargeq}{3}{matha}{"A9}

\renewcommand{\leq}{\varleq}
\renewcommand{\geq}{\vargeq}

% Datatool

\usepackage{datatool}
\usepackage{hyperref}

% Title page

\newcommand{\titlebox}{
  {\color{white}x} \\
  {\Huge\textsf{\textbf{Algebra 1 Book \booknumber}}} \\
  {\color{white}x} \\
  {\Large\textsf{\textbf{Digital Harbor High School}}} \\
  {\color{white}x} \\
  {\Large\textsf{\textbf{2024-2025}}} \\
}

\newcommand{\TitlePage}{
\vspace*{\fill}

\begin{center}
\begin{tikzpicture}[scale=1]
  \begin{axis}[
    width=\textwidth,
    height=\textheight,
    xmin=-2,
    xmax=17,
    ymin=-2,
    ymax=23,
    ytick={-5, ..., 25},
    xticklabel=\empty,
    yticklabel=\empty
    ]
    \addplot[samples=51, domain=2.6:7.4]{(x-5)^2 + 2};
    \addplot[red, samples=51, domain=2:8]{(3/3)*(x-4)+3};
    \filldraw[violet] (4,3) circle (4pt);
    \filldraw[violet] (7,6) circle (4pt);
    \addplot[samples=2, domain=1:6]{-2*(x - 3) + 18};
    \addplot[red, samples=2, domain=-1:9]{-(1/2)*(x - 3) + 18};
    \addplot[ForestGreen, samples=2, domain=3:9]{(x - 5) + 14};
    \filldraw[violet] (3,18) circle (4pt);
    \filldraw[violet] (5,14) circle (4pt);
    \filldraw[violet] (7,16) circle (4pt);
    \path[pattern=north west lines, pattern color=violet!50]
      (3,18) -- (7,16) -- (5,14) -- cycle;
    \node[inner sep=1ex, blue, draw, fill=white, align=right, below right] (T) at (8,13) {\titlebox};
  \end{axis}
\end{tikzpicture}
\end{center}

\vspace*{\fill}
}
