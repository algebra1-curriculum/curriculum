#!/bin/bash

cd ~/curriculum/alpha
for n in {1..80};
do
  datatooltk --in master.dbtex --filter Lesson eq $n --name lesson$n.dbtex --output lesson$n.dbtex
done
