#!/bin/bash

cd ~/curriculum/alpha
for n in {1..80};
do
  datatooltk --in master.dbtex --filter Lesson eq $n --name lesson$n.dbtex --output lesson$n.dbtex
  cd daily
  rm lesson$n-$(date --iso-8601).tex
  touch lesson$n-$(date --iso-8601).tex
  echo "\documentclass[11pt, twoside]{report}" >> lesson$n-$(date --iso-8601).tex
  echo "\usepackage{daily-preamble}" >> lesson$n-$(date --iso-8601).tex
  echo "\DTLloaddbtex{\problemDB}{../lesson$n.dbtex}" >> lesson$n-$(date --iso-8601).tex
  echo "\begin{document}" >> lesson$n-$(date --iso-8601).tex
  echo "\section{Lesson $n}" >> lesson$n-$(date --iso-8601).tex
  echo "\DTLforeach*{\problemDB}{\Cycle=Cycle, \Lesson=Lesson, \Problem=Problem, \Exercise=Exercise}{" >> lesson$n-$(date --iso-8601).tex
  echo "\renewcommand{\theexercise}{\Lesson.\Problem}" >> lesson$n-$(date --iso-8601).tex
  echo "\begin{exercise}" >> lesson$n-$(date --iso-8601).tex
  echo "\Exercise" >> lesson$n-$(date --iso-8601).tex
  echo "\end{exercise}}" >> lesson$n-$(date --iso-8601).tex
  echo "\end{document}" >> lesson$n-$(date --iso-8601).tex
  latexmk lesson$n-$(date --iso-8601).tex
  pdflatex lesson$n-$(date --iso-8601).tex
done
