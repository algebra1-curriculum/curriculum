* plan for 60-minute lessons
* leftover time: independent practice or more to next lesson
* four new atoms plus four review atoms per lesson 

* usability features:
  - in multi-part problems, each graph is labeled with a letter for ease of reference (teachers should direct students' attention to "graph E" rather than "the third graph on in the second row")


* code
  - B basic
  - L linear
  - Q quadratic
  - E exponential

* include retrieval strength in code:
  - N new
  - RD retrieved a week ago
  - RW retrieved a week ago
  - RM retrieved a month ago
  - C consolidation task

* at bats:
  - N
  - N+1 through N+4
  - N+9, N+10
  - N+17, N+18, N+19
  - N+30
  - N+40
  - N+55
  - N+80

* interleaving
  - after completing threads, create exercises that require discrimination between strategies

* front cover ideas
  - graph of quadratic with axis of symmetry, symmetric points, intercepts
  - graph of system of inequalities
  - tables with arrows: linear and exponential

* mastery test
  - one spreadsheet for all teachers' results

* additional resources
  - MCAP reference sheet
  - multiplication table
  - perfect squares and square roots
  - forms of quadratic equations
  - ruler
