# 2024-07-01

* how should we teach 5 - (-3)?
* standard form quadratics: find a, b, c
* complete the square: circle the perfect square trinomials, then factor them

# half-formed ideas

* matching: linear graphs with six pairs 
* matching: systems of inequalities with six pairs
* label problems for initial teaching and expanded teaching
* invisible zeros and ones:
  - if this equation is in point-slope form, what's the missing number?
  - if this equation is in vertex form, what's the missing number?
* collate into lessons, then designate display-only problems and workbook problems
  - workbook problems are less time efficient than whiteboards:
    * barrier for entry: students won't start until they feel confident they know how to work the problem, but they can't gain confidence without working more problems
    * students will straggle behind copying answers
    * if teacher wants reliable data, workbook problems must be checked one by one, or reiterated as choral response/whiteboard problems
    * varying the modality of engagement creates the illusion of change of pace -- all workbook all the time feels stagnant
* angle brackets for function notation as early scaffold

* use vocabulary rather than symbols
  - leading coefficient (a)
  - scale factor (|a|)
  - axis of symmetry (h)
  - extremum (k)
  - input/horizontal variable (x)
  - output/vertical variable (y)
  - root/horizontal intercept (x-intercept, r, s)
  - vertical intercept (y-intercept, c)
* idea: vocabulary provides contextual clues that symbols don't, and builds and consolidates schemata; the only additional cost is a few extra syllables

# 2024-07-08

# done

* terminology: base point for point-slope form
* matching: parabolas with six pairs 
* scale factor: "opens up fast"
* matching tasks design
* graph colors
* shaded boxes for fill-in the blank
* vertical intercept and horizontal intercept
* mixed numbers?
* introduce function notation into graphing thread: graph f(x) = 3
